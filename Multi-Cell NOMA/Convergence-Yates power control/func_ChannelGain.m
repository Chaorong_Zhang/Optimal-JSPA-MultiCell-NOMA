function [h,h_cell] = func_ChannelGain(B,U,M,Cor_BS,Cor_user)
%% Distances
%Distance between users and BSs
Dis_user_BS=ones(B,B,M);
for j=1:B
    for b=1:B
        for m=1:U(b)
            Dis_user_BS(j,b,m)=abs (Cor_BS(j)-Cor_user(b,m));
        end
    end
end
%% 3GPP Model
path_exp=ones(B,B,M);
for j=1:B
    for b=1:B
        for m=1:U(b)
            if (j==1)
                path_exp(j,b,m)=128.1+37.6*log10(Dis_user_BS(j,b,m)/1000)+8*randn;
            else path_exp(j,b,m)=140.7+36.7*log10(Dis_user_BS(j,b,m)/1000)+8*randn;
            end
        end
    end
end
var=10.^(-path_exp/10);
%%Rayleigh+pathloss channel gain
h = sqrt(var/2).*(randn(B,B,M)+i*randn(B,B,M));
h=abs(h).^2; % channel power gain

for j=1:B
    for b=1:B
        for m=1:M
            if (m>U(b))
                h(j,b,m)=0;
            end
        end
    end
end

for b=1:B
    h_cell(b,:)=h(b,b,:);
end
