function [R_min]=func_minrate(B,U,M,R_macro,R_femto)
R_min=zeros(B,M);
for b=1:B
    for m=1:U(b)
        if (b==1)
           R_min(b,m)=R_macro; 
        else R_min(b,m)=R_femto;
        end
    end
end