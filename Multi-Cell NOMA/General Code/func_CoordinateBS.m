function [Cor_BS] = func_CoordinateBS(Dis_BS)
%% Coordinate of BSs
    angle_BS=[0]; %angle of FBS
    Cor_BS=[0,Dis_BS.*( cos(angle_BS) + i*sin(angle_BS) )]; %Coordinated of BSs
