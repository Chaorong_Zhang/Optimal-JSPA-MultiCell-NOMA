function [SIC_order_cell]=func_SICordering_cell(b,U,M,h_norm)
SIC_order_cell=zeros(1,M); %decoding order based on updated normalized channel gains, 
%higher value corresponds to the higher decoding order

    count=0;
    while(count <= U(b)-1)
        stronguser=find(h_norm(b,:)==max(h_norm(b,:)));
        SIC_order_cell(1,stronguser)=U(b)-count;
        count=count+1;
        h_norm(b,stronguser)=0;
    end

