clear all
clc
%% System Settings
num_Mcell_user=[2:4]; %number of macro-cell users
num_Fcell_user=[2:6]; %number of femto-cell users
Distance_BS=200; %distance between macro-cell and femto-cell
BW=5e+6; %wireless bandwidth
PSD_noise=-174; %PSD of AWGN in dBm/Hz
PSD_noise=10.^((PSD_noise-30)/10); %PSD of Noise in watts
max_samp=1000; %number of samples
fprintf('***Total Samples***\n\n')
disp(length(num_Mcell_user)*length(num_Fcell_user)*length(Distance_BS)...
    *max_samp)
for clstr_m=1:length(num_Mcell_user)
    for clstr_f=1:length(num_Fcell_user)
        for sampl=1:max_samp 
            %% Simulation Settings
            %%%%%Network Topology
            Dis_BS=Distance_BS;
            Cor_BS=func_CoordinateBS(Dis_BS);
            B=numel(Cor_BS); %Number of BSs
            radius_BS=[500,40*ones(1,B-1)]; %radius of each BS in meter
            Macro_cell=num_Mcell_user(clstr_m);
            Femto_cell=num_Fcell_user(clstr_f);
            U=[Macro_cell,Femto_cell*ones(1,B-1)];
            M=max(U);
            min_Dis_User=[20,2*ones(1,B-1)]; %minimum distance of user to associated BS in meter
            %%%%%Coordinate of users
            Cor_user=func_CoordinateUser(B,Cor_BS,U,M,radius_BS,min_Dis_User);
            [h,h_cell]=func_ChannelGain(B,U,M,Cor_BS,Cor_user);
            h_sample{clstr_m,clstr_f,sampl}=h; %save sample channel gains
            h_cell_sample{clstr_m,clstr_f,sampl}=h_cell; %save sample channel gains
            %Noise power
            N0=abs(randn(B,M)).*PSD_noise*BW; %AWGN power at users
            AWGN_sample{clstr_m,clstr_f,sampl}=N0; %save sample AWGN power at users
        end
    end
end
save('STEP1_Settings_MultiCell','B','AWGN_sample','h_sample','h_cell_sample','num_Mcell_user',...
    'num_Fcell_user','Distance_BS','PSD_noise','max_samp')