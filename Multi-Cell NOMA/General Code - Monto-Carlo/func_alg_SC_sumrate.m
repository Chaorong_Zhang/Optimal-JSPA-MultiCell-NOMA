function [p_SC,r_SC,alpha_SC,SIC_order_SC,Rtot_SC,Feasible_SC]=...
    func_alg_SC_sumrate(B,U,M,R_min,P_max,ceil_amin,ICI_indctr,h,h_cell,N0,eps_a)
%Initialization
alpha=1*ones(1,B);
p_SC=0;
r_SC=0;
alpha_SC=0;
SIC_order_SC=0;
Rtot_SC=0;
Feasible_SC=0;
for b=1:B
    for i=1:U(b)
        R_minextra(b,i)=R_min(b,i)+0.0001;
    end
end
while alpha(1)>ceil_amin(1)
    ICI=reshape(sum(repmat(alpha'.*P_max',1,B,M).*ICI_indctr.*h,1),B,M);
    h_norm=h_cell./(ICI+N0);    
    [SIC_order]=func_SICordering(B,U,M,h_norm);    
    [p]=func_optpower_sumrate(B,U,M,h_norm,R_minextra,SIC_order,P_max,alpha);
    [r]=func_rate(B,U,M,SIC_order,p,h_norm);
    if ( min(min(r>=R_min))==1 && sum(r,'all') > Rtot_SC )
        Feasible_SC=1;
        Rtot_SC=sum(r,'all');
        p_SC=p;
        r_SC=r;
        SIC_order_SC=SIC_order;
        alpha_SC=alpha;
    end
    %Update (reduce) alpha(1)
    alpha(1)=alpha(1)-eps_a;
end
