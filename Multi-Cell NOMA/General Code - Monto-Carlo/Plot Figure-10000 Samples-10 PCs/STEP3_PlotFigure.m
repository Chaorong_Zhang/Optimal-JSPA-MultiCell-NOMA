clear all
clc
set(0,'DefaultFigureVisible','off') %do not display figures
%set(0,'DefaultFigureVisible','on'); %display figures
%% Outage Probability: JSPA, JRPA, FRPA

%%% Order of NOMA Clusters: Matrix(clstr_m,cls_f,r_m,r_f,sampl)
clear all
clc
%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219b','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud219b{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud219b{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud219b{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219q','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud219q{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud219q{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud219q{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219x','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud219x{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud219x{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud219x{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219t','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud219t{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud219t{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud219t{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320b','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud320b{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud320b{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud320b{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320e','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud320e{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud320e{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud320e{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320g','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud320g{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud320g{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud320g{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320i','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud320i{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud320i{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud320i{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320j','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud320j{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud320j{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud320j{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320n','Feasible_JSPA_sample','Feasible_JRPA_sample','Feasible_FRPA_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA_stud320n{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_JRPA_stud320n{cls_m}=1-reshape(sum(Feasible_JRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_FRPA_stud320n{cls_m}=1-reshape(sum(Feasible_FRPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end

%Merging PCs results
for cls_m=1:length(num_Mcell_user)
    outage_cluster_JSPA{cls_m}=(outage_cluster_JSPA_stud219b{cls_m}+outage_cluster_JSPA_stud219q{cls_m}+...
        outage_cluster_JSPA_stud219x{cls_m}+outage_cluster_JSPA_stud219t{cls_m}+...
        outage_cluster_JSPA_stud320b{cls_m}+outage_cluster_JSPA_stud320e{cls_m}+...
        outage_cluster_JSPA_stud320g{cls_m}+outage_cluster_JSPA_stud320i{cls_m}+...
        outage_cluster_JSPA_stud320j{cls_m}+outage_cluster_JSPA_stud320n{cls_m})/10;
    outage_cluster_JRPA{cls_m}=(outage_cluster_JRPA_stud219b{cls_m}+outage_cluster_JRPA_stud219q{cls_m}+...
        outage_cluster_JRPA_stud219x{cls_m}+outage_cluster_JRPA_stud219t{cls_m}+...
        outage_cluster_JRPA_stud320b{cls_m}+outage_cluster_JRPA_stud320e{cls_m}+...
        outage_cluster_JRPA_stud320g{cls_m}+outage_cluster_JRPA_stud320i{cls_m}+...
        outage_cluster_JRPA_stud320j{cls_m}+outage_cluster_JRPA_stud320n{cls_m})/10;
    outage_cluster_FRPA{cls_m}=(outage_cluster_FRPA_stud219b{cls_m}+outage_cluster_FRPA_stud219q{cls_m}+...
        outage_cluster_FRPA_stud219x{cls_m}+outage_cluster_FRPA_stud219t{cls_m}+...
        outage_cluster_FRPA_stud320b{cls_m}+outage_cluster_FRPA_stud320e{cls_m}+...
        outage_cluster_FRPA_stud320g{cls_m}+outage_cluster_FRPA_stud320i{cls_m}+...
        outage_cluster_FRPA_stud320j{cls_m}+outage_cluster_FRPA_stud320n{cls_m})/10;
end

Fig_outage_Algs_usernum=figure;
hold on
plot(num_Fcell_user,outage_cluster_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,outage_cluster_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,outage_cluster_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,outage_cluster_JRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,outage_cluster_JRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,outage_cluster_JRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,outage_cluster_FRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(num_Fcell_user,outage_cluster_FRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(num_Fcell_user,outage_cluster_FRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('JSPA-Opt, |U_m|=2','JSPA-Opt, |U_m|=3','JSPA-Opt, |U_m|=4',...
       'JRPA-CNR, |U_m|=2','JRPA-CNR, |U_m|=3','JRPA-CNR, |U_m|=4',...
       'FRPA-CNR, |U_m|=2','FRPA-CNR, |U_m|=3','FRPA-CNR, |U_m|=4',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('|U_f|','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Outage probability','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',num_Fcell_user,'YScale', 'log','FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_outage_Algs_usernum,'Fig_outage_Algs_usernum')

%%% Minimim Rate: Matrix(clstr_m,cls_f,r_m,r_f,sampl)

%%%M==2 for each cell
clear all
clc
%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219b','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud219b{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud219b{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud219b{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219q','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud219q{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud219q{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud219q{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219x','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud219x{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud219x{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud219x{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219t','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud219t{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud219t{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud219t{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320b','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320b{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320b{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320b{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320e','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320e{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320e{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320e{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320g','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320g{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320g{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320g{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320i','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320i{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320i{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320i{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320j','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320j{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320j{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320j{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320n','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320n{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320n{r_m}=1-reshape(sum(Feasible_JRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320n{r_m}=1-reshape(sum(Feasible_FRPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA{r_m}=(outage_minrate_JSPA_stud219b{r_m}+outage_minrate_JSPA_stud219q{r_m}+...
        outage_minrate_JSPA_stud219x{r_m}+outage_minrate_JSPA_stud219t{r_m}+...
        outage_minrate_JSPA_stud320b{r_m}+outage_minrate_JSPA_stud320e{r_m}+...
        outage_minrate_JSPA_stud320g{r_m}+outage_minrate_JSPA_stud320i{r_m}+...
        outage_minrate_JSPA_stud320j{r_m}+outage_minrate_JSPA_stud320n{r_m})/10;
    outage_minrate_JRPA{r_m}=(outage_minrate_JRPA_stud219b{r_m}+outage_minrate_JRPA_stud219q{r_m}+...
        outage_minrate_JRPA_stud219x{r_m}+outage_minrate_JRPA_stud219t{r_m}+...
        outage_minrate_JRPA_stud320b{r_m}+outage_minrate_JRPA_stud320e{r_m}+...
        outage_minrate_JRPA_stud320g{r_m}+outage_minrate_JRPA_stud320i{r_m}+...
        outage_minrate_JRPA_stud320j{r_m}+outage_minrate_JRPA_stud320n{r_m})/10;
    outage_minrate_FRPA{r_m}=(outage_minrate_FRPA_stud219b{r_m}+outage_minrate_FRPA_stud219q{r_m}+...
        outage_minrate_FRPA_stud219x{r_m}+outage_minrate_FRPA_stud219t{r_m}+...
        outage_minrate_FRPA_stud320b{r_m}+outage_minrate_FRPA_stud320e{r_m}+...
        outage_minrate_FRPA_stud320g{r_m}+outage_minrate_FRPA_stud320i{r_m}+...
        outage_minrate_FRPA_stud320j{r_m}+outage_minrate_FRPA_stud320n{r_m})/10;
end
Fig_outage_Algs_minrate_2user=figure;
hold on
plot(rate_min_f,outage_minrate_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_JRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_JRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_JRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_FRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,outage_minrate_FRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,outage_minrate_FRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('JSPA-Opt, R^{min}_m=0.5 bps/Hz','JSPA-Opt, R^{min}_m=1 bps/Hz','JSPA-Opt, R^{min}_m=2 bps/Hz',...
       'JRPA-CNR, R^{min}_m=0.5 bps/Hz','JRPA-CNR, R^{min}_m=1 bps/Hz','JRPA-CNR, R^{min}_m=2 bps/Hz',...
       'FRPA-CNR, R^{min}_m=0.5 bps/Hz','FRPA-CNR, R^{min}_m=1 bps/Hz','FRPA-CNR, R^{min}_m=2 bps/Hz',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Outage probability','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'YScale', 'log','FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_outage_Algs_minrate_2user,'Fig_outage_Algs_minrate_2user')

%%%M==3 for each cell
clear all
clc
%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219b','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud219b{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud219b{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud219b{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219q','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud219q{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud219q{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud219q{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219x','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud219x{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud219x{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud219x{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219t','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud219t{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud219t{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud219t{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320b','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320b{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320b{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320b{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320e','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320e{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320e{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320e{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320g','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320g{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320g{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320g{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320i','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320i{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320i{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320i{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320j','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320j{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320j{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320j{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320n','rate_min_m','rate_min_f','Feasible_JSPA_sample',...
    'Feasible_JRPA_sample','Feasible_FRPA_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA_stud320n{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_JRPA_stud320n{r_m}=1-reshape(sum(Feasible_JRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_FRPA_stud320n{r_m}=1-reshape(sum(Feasible_FRPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    outage_minrate_JSPA{r_m}=(outage_minrate_JSPA_stud219b{r_m}+outage_minrate_JSPA_stud219q{r_m}+...
        outage_minrate_JSPA_stud219x{r_m}+outage_minrate_JSPA_stud219t{r_m}+...
        outage_minrate_JSPA_stud320b{r_m}+outage_minrate_JSPA_stud320e{r_m}+...
        outage_minrate_JSPA_stud320g{r_m}+outage_minrate_JSPA_stud320i{r_m}+...
        outage_minrate_JSPA_stud320j{r_m}+outage_minrate_JSPA_stud320n{r_m})/10;
    outage_minrate_JRPA{r_m}=(outage_minrate_JRPA_stud219b{r_m}+outage_minrate_JRPA_stud219q{r_m}+...
        outage_minrate_JRPA_stud219x{r_m}+outage_minrate_JRPA_stud219t{r_m}+...
        outage_minrate_JRPA_stud320b{r_m}+outage_minrate_JRPA_stud320e{r_m}+...
        outage_minrate_JRPA_stud320g{r_m}+outage_minrate_JRPA_stud320i{r_m}+...
        outage_minrate_JRPA_stud320j{r_m}+outage_minrate_JRPA_stud320n{r_m})/10;
    outage_minrate_FRPA{r_m}=(outage_minrate_FRPA_stud219b{r_m}+outage_minrate_FRPA_stud219q{r_m}+...
        outage_minrate_FRPA_stud219x{r_m}+outage_minrate_FRPA_stud219t{r_m}+...
        outage_minrate_FRPA_stud320b{r_m}+outage_minrate_FRPA_stud320e{r_m}+...
        outage_minrate_FRPA_stud320g{r_m}+outage_minrate_FRPA_stud320i{r_m}+...
        outage_minrate_FRPA_stud320j{r_m}+outage_minrate_FRPA_stud320n{r_m})/10;
end
Fig_outage_Algs_minrate_3user=figure;
hold on
plot(rate_min_f,outage_minrate_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_JRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_JRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_JRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_FRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,outage_minrate_FRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,outage_minrate_FRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('JSPA-Opt, R^{min}_m=0.5 bps/Hz','JSPA-Opt, R^{min}_m=1 bps/Hz','JSPA-Opt, R^{min}_m=2 bps/Hz',...
       'JRPA-CNR, R^{min}_m=0.5 bps/Hz','JRPA-CNR, R^{min}_m=1 bps/Hz','JRPA-CNR, R^{min}_m=2 bps/Hz',...
       'FRPA-CNR, R^{min}_m=0.5 bps/Hz','FRPA-CNR, R^{min}_m=1 bps/Hz','FRPA-CNR, R^{min}_m=2 bps/Hz',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Outage probability','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'YScale', 'log','FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_outage_Algs_minrate_3user,'Fig_outage_Algs_minrate_3user')

%% Outage Probability: Centralized, Semi-Centralized, Distributed Frameworks

%%% Order of NOMA Clusters: Matrix(clstr_m,cls_f,r_m,r_f,sampl)
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219b','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud219b','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud219b{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud219b{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud219b{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219q','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud219q','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud219q{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud219q{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud219q{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219t','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud219t','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud219t{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud219t{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud219t{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219x','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud219x','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud219x{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud219x{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud219x{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320b','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320b','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud320b{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud320b{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud320b{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320e','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320e','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud320e{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud320e{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud320e{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320g','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320g','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud320g{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud320g{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud320g{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320i','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320i','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud320i{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud320i{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud320i{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320j','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320j','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud320j{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud320j{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud320j{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320n','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320n','Feasible_DF_sample','Feasible_SC_sample')
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent_stud320n{cls_m}=1-reshape(sum(Feasible_JSPA_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_SC_stud320n{cls_m}=1-reshape(sum(Feasible_SC_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    outage_cluster_DF_stud320n{cls_m}=1-reshape(sum(Feasible_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end

%Merging PCs results
for cls_m=1:length(num_Mcell_user)
    outage_cluster_cent{cls_m}=(outage_cluster_cent_stud219b{cls_m}+outage_cluster_cent_stud219q{cls_m}+...
        outage_cluster_cent_stud219t{cls_m}+outage_cluster_cent_stud219x{cls_m}+...
        outage_cluster_cent_stud320b{cls_m}+outage_cluster_cent_stud320e{cls_m}+...
        outage_cluster_cent_stud320g{cls_m}+outage_cluster_cent_stud320i{cls_m}+...
        outage_cluster_cent_stud320j{cls_m}+outage_cluster_cent_stud320n{cls_m})/10;
    outage_cluster_SC{cls_m}=(outage_cluster_SC_stud219b{cls_m}+outage_cluster_SC_stud219q{cls_m}+...
        outage_cluster_SC_stud219t{cls_m}+outage_cluster_SC_stud219x{cls_m}+...
        outage_cluster_SC_stud320b{cls_m}+outage_cluster_SC_stud320e{cls_m}+...
        outage_cluster_SC_stud320g{cls_m}+outage_cluster_SC_stud320i{cls_m}+...
        outage_cluster_SC_stud320j{cls_m}+outage_cluster_SC_stud320n{cls_m})/10;
    outage_cluster_DF{cls_m}=(outage_cluster_DF_stud219b{cls_m}+outage_cluster_DF_stud219q{cls_m}+...
        outage_cluster_DF_stud219t{cls_m}+outage_cluster_DF_stud219x{cls_m}+...
        outage_cluster_DF_stud320b{cls_m}+outage_cluster_DF_stud320e{cls_m}+...
        outage_cluster_DF_stud320g{cls_m}+outage_cluster_DF_stud320i{cls_m}+...
        outage_cluster_DF_stud320j{cls_m}+outage_cluster_DF_stud320n{cls_m})/10;
end
Fig_outage_FW_usernum=figure;
hold on
plot(num_Fcell_user,outage_cluster_cent{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,outage_cluster_cent{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,outage_cluster_cent{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,outage_cluster_SC{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,outage_cluster_SC{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,outage_cluster_SC{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,outage_cluster_DF{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(num_Fcell_user,outage_cluster_DF{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(num_Fcell_user,outage_cluster_DF{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('Centralized, |U_m|=2','Centralized, |U_m|=3','Centralized, |U_m|=4',...
       'Semi-Centralized, |U_m|=2','Semi-Centralized, |U_m|=3','Semi-Centralized, |U_m|=4',...
       'Distributed, |U_m|=2','Distributed, |U_m|=3','Distributed, |U_m|=4',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('|U_f|','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Outage probability','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',num_Fcell_user,'YScale', 'log','FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_outage_FW_usernum,'Fig_outage_FW_usernum')

%%% Minimim Rate: Matrix(clstr_m,cls_f,r_m,r_f,sampl)

%%%M==2 for each cell
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219b','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219b','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud219b{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud219b{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud219b{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219q','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219q','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud219q{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud219q{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud219q{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219t','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219t','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud219t{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud219t{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud219t{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219x','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219x','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud219x{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud219x{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud219x{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320b','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320b','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320b{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320b{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320b{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320e','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320e','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320e{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320e{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320e{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320g','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320g','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320g{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320g{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320g{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320i','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320i','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320i{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320i{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320i{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320j','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320j','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320j{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320j{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320j{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320n','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320n','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320n{r_m}=1-reshape(sum(Feasible_JSPA_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320n{r_m}=1-reshape(sum(Feasible_SC_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320n{r_m}=1-reshape(sum(Feasible_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    outage_minrate_cent{r_m}=(outage_minrate_cent_stud219b{r_m}+outage_minrate_cent_stud219q{r_m}+...
        outage_minrate_cent_stud219t{r_m}+outage_minrate_cent_stud219x{r_m}+...
        outage_minrate_cent_stud320b{r_m}+outage_minrate_cent_stud320e{r_m}+...
        outage_minrate_cent_stud320g{r_m}+outage_minrate_cent_stud320i{r_m}+...
        outage_minrate_cent_stud320j{r_m}+outage_minrate_cent_stud320n{r_m})/10;
    outage_minrate_SC{r_m}=(outage_minrate_SC_stud219b{r_m}+outage_minrate_SC_stud219q{r_m}+...
        outage_minrate_SC_stud219t{r_m}+outage_minrate_SC_stud219x{r_m}+...
        outage_minrate_SC_stud320b{r_m}+outage_minrate_SC_stud320e{r_m}+...
        outage_minrate_SC_stud320g{r_m}+outage_minrate_SC_stud320i{r_m}+...
        outage_minrate_SC_stud320j{r_m}+outage_minrate_SC_stud320n{r_m})/10;
    outage_minrate_DF{r_m}=(outage_minrate_DF_stud219b{r_m}+outage_minrate_DF_stud219q{r_m}+...
        outage_minrate_DF_stud219t{r_m}+outage_minrate_DF_stud219x{r_m}+...
        outage_minrate_DF_stud320b{r_m}+outage_minrate_DF_stud320e{r_m}+...
        outage_minrate_DF_stud320g{r_m}+outage_minrate_DF_stud320i{r_m}+...
        outage_minrate_DF_stud320j{r_m}+outage_minrate_DF_stud320n{r_m})/10;
end
Fig_outage_FW_minrate_2user=figure;
hold on
plot(rate_min_f,outage_minrate_cent{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_cent{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_cent{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_SC{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_SC{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_SC{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_DF{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,outage_minrate_DF{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,outage_minrate_DF{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('Centralized, R^{min}_m=0.5 bps/Hz','Centralized, R^{min}_m=1 bps/Hz','Centralized, R^{min}_m=2 bps/Hz',...
       'Semi-Centralized, R^{min}_m=0.5 bps/Hz','Semi-Centralized, R^{min}_m=1 bps/Hz','Semi-Centralized, R^{min}_m=2 bps/Hz',...
       'Distributed, R^{min}_m=0.5 bps/Hz','Distributed, R^{min}_m=1 bps/Hz','Distributed, R^{min}_m=2 bps/Hz',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Outage probability','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'YScale', 'log','FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_outage_FW_minrate_2user,'Fig_outage_FW_minrate_2user')

%%%M==3 for each cell
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219b','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219b','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud219b{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud219b{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud219b{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219q','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219q','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud219q{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud219q{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud219q{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219t','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219t','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud219t{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud219t{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud219t{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219x','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219x','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud219x{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud219x{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud219x{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320b','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320b','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320b{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320b{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320b{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320e','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320e','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320e{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320e{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320e{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320g','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320g','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320g{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320g{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320g{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320i','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320i','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320i{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320i{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320i{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320j','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320j','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320j{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320j{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320j{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320n','rate_min_m','rate_min_f','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320n','Feasible_DF_sample','Feasible_SC_sample')
for r_m=1:length(rate_min_m)
    outage_minrate_cent_stud320n{r_m}=1-reshape(sum(Feasible_JSPA_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_SC_stud320n{r_m}=1-reshape(sum(Feasible_SC_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    outage_minrate_DF_stud320n{r_m}=1-reshape(sum(Feasible_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    outage_minrate_cent{r_m}=(outage_minrate_cent_stud219b{r_m}+outage_minrate_cent_stud219q{r_m}+...
        outage_minrate_cent_stud219t{r_m}+outage_minrate_cent_stud219x{r_m}+...
        outage_minrate_cent_stud320b{r_m}+outage_minrate_cent_stud320e{r_m}+...
        outage_minrate_cent_stud320g{r_m}+outage_minrate_cent_stud320i{r_m}+...
        outage_minrate_cent_stud320j{r_m}+outage_minrate_cent_stud320n{r_m})/10;
    outage_minrate_SC{r_m}=(outage_minrate_SC_stud219b{r_m}+outage_minrate_SC_stud219q{r_m}+...
        outage_minrate_SC_stud219t{r_m}+outage_minrate_SC_stud219x{r_m}+...
        outage_minrate_SC_stud320b{r_m}+outage_minrate_SC_stud320e{r_m}+...
        outage_minrate_SC_stud320g{r_m}+outage_minrate_SC_stud320i{r_m}+...
        outage_minrate_SC_stud320j{r_m}+outage_minrate_SC_stud320n{r_m})/10;
    outage_minrate_DF{r_m}=(outage_minrate_DF_stud219b{r_m}+outage_minrate_DF_stud219q{r_m}+...
        outage_minrate_DF_stud219t{r_m}+outage_minrate_DF_stud219x{r_m}+...
        outage_minrate_DF_stud320b{r_m}+outage_minrate_DF_stud320e{r_m}+...
        outage_minrate_DF_stud320g{r_m}+outage_minrate_DF_stud320i{r_m}+...
        outage_minrate_DF_stud320j{r_m}+outage_minrate_DF_stud320n{r_m})/10;
end

Fig_outage_FW_minrate_3user=figure;
hold on
plot(rate_min_f,outage_minrate_cent{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_cent{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_cent{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,outage_minrate_SC{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_SC{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_SC{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,outage_minrate_DF{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,outage_minrate_DF{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,outage_minrate_DF{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('Centralized, R^{min}_m=0.5 bps/Hz','Centralized, R^{min}_m=1 bps/Hz','Centralized, R^{min}_m=2 bps/Hz',...
       'Semi-Centralized, R^{min}_m=0.5 bps/Hz','Semi-Centralized, R^{min}_m=1 bps/Hz','Semi-Centralized, R^{min}_m=2 bps/Hz',...
       'Distributed, R^{min}_m=0.5 bps/Hz','Distributed, R^{min}_m=1 bps/Hz','Distributed, R^{min}_m=2 bps/Hz',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Outage probability','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'YScale', 'log','FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_outage_FW_minrate_3user,'Fig_outage_FW_minrate_3user')

%% Sum-Rate Maximization Problem: Total Spectral Efficiency of JSPA, JRPA, FRPA
%Order of NOMA cluster
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud219b','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud219b{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219b{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219b{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud219q','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud219q{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219q{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219q{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud219t','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud219t{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219t{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219t{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud219x','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud219x{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219x{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219x{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320b','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320b{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320b{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320b{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320e','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320e{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320e{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320e{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320g','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320g{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320g{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320g{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320i','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320i{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320i{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320i{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320j','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320j{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320j{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320j{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320n','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320n{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320n{cls_m}=reshape(sum(R_tot_JRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320n{cls_m}=reshape(sum(R_tot_FRPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end

%Merging PCs results
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA{cls_m}=(R_tot_JSPA_stud219b{cls_m}+R_tot_JSPA_stud219q{cls_m}+...
        R_tot_JSPA_stud219t{cls_m}+R_tot_JSPA_stud219x{cls_m}+...
        R_tot_JSPA_stud320b{cls_m}+R_tot_JSPA_stud320e{cls_m}+...
        R_tot_JSPA_stud320g{cls_m}+R_tot_JSPA_stud320i{cls_m}+...
        R_tot_JSPA_stud320j{cls_m}+R_tot_JSPA_stud320n{cls_m})/10;
    R_tot_JRPA{cls_m}=(R_tot_JRPA_stud219b{cls_m}+R_tot_JRPA_stud219q{cls_m}+...
        R_tot_JRPA_stud219t{cls_m}+R_tot_JRPA_stud219x{cls_m}+...
        R_tot_JRPA_stud320b{cls_m}+R_tot_JRPA_stud320e{cls_m}+...
        R_tot_JRPA_stud320g{cls_m}+R_tot_JRPA_stud320i{cls_m}+...
        R_tot_JRPA_stud320j{cls_m}+R_tot_JRPA_stud320n{cls_m})/10;
    R_tot_FRPA{cls_m}=(R_tot_FRPA_stud219b{cls_m}+R_tot_FRPA_stud219q{cls_m}+...
        R_tot_FRPA_stud219t{cls_m}+R_tot_FRPA_stud219x{cls_m}+...
        R_tot_FRPA_stud320b{cls_m}+R_tot_FRPA_stud320e{cls_m}+...
        R_tot_FRPA_stud320g{cls_m}+R_tot_FRPA_stud320i{cls_m}+...
        R_tot_FRPA_stud320j{cls_m}+R_tot_FRPA_stud320n{cls_m})/10;
end
Fig_Rtot_usernum=figure;
hold on
plot(num_Fcell_user,R_tot_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,R_tot_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,R_tot_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,R_tot_JRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,R_tot_JRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,R_tot_JRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,R_tot_FRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(num_Fcell_user,R_tot_FRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(num_Fcell_user,R_tot_FRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('JSPA-Opt, |U_m|=2','JSPA-Opt, |U_m|=3','JSPA-Opt, |U_m|=4',...
       'JRPA-CNR, |U_m|=2','JRPA-CNR, |U_m|=3','JRPA-CNR, |U_m|=4',...
       'FRPA-CNR, |U_m|=2','FRPA-CNR, |U_m|=3','FRPA-CNR, |U_m|=4',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('|U_f|','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total spectral efficiency (bps/Hz)','FontSize',14,...
    'FontName','Times New Roman')
set(gca,'XTick',num_Fcell_user,'FontName','Times New Roman','FontSize',14);
grid on
savefig(Fig_Rtot_usernum,'Fig_Rtot_usernum')

%%Minimim Rate: Matrix(clstr_m,cls_f,r_m,r_f,sampl)

%%%M==2 for each cell
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219b','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219b{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219b{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219b{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219q','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219q{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219q{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219q{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219t','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219t{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219t{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219t{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219x','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219x{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219x{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219x{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320b','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320b{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320b{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320b{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320e','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320e{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320e{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320e{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320g','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320g{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320g{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320g{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320i','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320i{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320i{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320i{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320j','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320j{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320j{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320j{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320n','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320n{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320n{r_m}=reshape(sum(R_tot_JRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320n{r_m}=reshape(sum(R_tot_FRPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    R_tot_JSPA{r_m}=(R_tot_JSPA_stud219b{r_m}+R_tot_JSPA_stud219q{r_m}+...
        R_tot_JSPA_stud219t{r_m}+R_tot_JSPA_stud219x{r_m}+...
        R_tot_JSPA_stud320b{r_m}+R_tot_JSPA_stud320e{r_m}+...
        R_tot_JSPA_stud320g{r_m}+R_tot_JSPA_stud320i{r_m}+...
        R_tot_JSPA_stud320j{r_m}+R_tot_JSPA_stud320n{r_m})/10;
    R_tot_JRPA{r_m}=(R_tot_JRPA_stud219b{r_m}+R_tot_JRPA_stud219q{r_m}+...
        R_tot_JRPA_stud219t{r_m}+R_tot_JRPA_stud219x{r_m}+...
        R_tot_JRPA_stud320b{r_m}+R_tot_JRPA_stud320e{r_m}+...
        R_tot_JRPA_stud320g{r_m}+R_tot_JRPA_stud320i{r_m}+...
        R_tot_JRPA_stud320j{r_m}+R_tot_JRPA_stud320n{r_m})/10;
    R_tot_FRPA{r_m}=(R_tot_FRPA_stud219b{r_m}+R_tot_FRPA_stud219q{r_m}+...
        R_tot_FRPA_stud219t{r_m}+R_tot_FRPA_stud219x{r_m}+...
        R_tot_FRPA_stud320b{r_m}+R_tot_FRPA_stud320e{r_m}+...
        R_tot_FRPA_stud320g{r_m}+R_tot_FRPA_stud320i{r_m}+...
        R_tot_FRPA_stud320j{r_m}+R_tot_FRPA_stud320n{r_m})/10;
end

Fig_Rtot_minrate_2user=figure;
hold on
plot(rate_min_f,R_tot_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_JRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_JRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_FRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,R_tot_FRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,R_tot_FRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('JSPA-Opt, R^{min}_m=0.5 bps/Hz','JSPA-Opt, R^{min}_m=1 bps/Hz','JSPA-Opt, R^{min}_m=2 bps/Hz',...
       'JRPA-CNR, R^{min}_m=0.5 bps/Hz','JRPA-CNR, R^{min}_m=1 bps/Hz','JRPA-CNR, R^{min}_m=2 bps/Hz',...
       'FRPA-CNR, R^{min}_m=0.5 bps/Hz','FRPA-CNR, R^{min}_m=1 bps/Hz','FRPA-CNR, R^{min}_m=2 bps/Hz',...
       'location','northeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total spectral efficiency (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_Rtot_minrate_2user,'Fig_Rtot_minrate_2user')

%%%M==3 for each cell
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219b','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219b{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219b{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219b{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219q','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219q{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219q{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219q{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219t','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219t{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219t{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219t{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219x','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219x{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud219x{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud219x{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320b','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320b{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320b{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320b{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320e','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320e{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320e{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320e{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320g','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320g{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320g{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320g{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320i','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320i{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320i{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320i{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320j','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320j{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320j{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320j{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320n','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_tot_JRPA_SM_sample','R_tot_FRPA_SM_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320n{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_JRPA_stud320n{r_m}=reshape(sum(R_tot_JRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_FRPA_stud320n{r_m}=reshape(sum(R_tot_FRPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end

for r_m=1:length(rate_min_m)
    R_tot_JSPA{r_m}=(R_tot_JSPA_stud219b{r_m}+R_tot_JSPA_stud219q{r_m}+...
        R_tot_JSPA_stud219t{r_m}+R_tot_JSPA_stud219x{r_m}+...
        R_tot_JSPA_stud320b{r_m}+R_tot_JSPA_stud320e{r_m}+...
        R_tot_JSPA_stud320g{r_m}+R_tot_JSPA_stud320i{r_m}+...
        R_tot_JSPA_stud320j{r_m}+R_tot_JSPA_stud320n{r_m})/10;
    R_tot_JRPA{r_m}=(R_tot_JRPA_stud219b{r_m}+R_tot_JRPA_stud219q{r_m}+...
        R_tot_JRPA_stud219t{r_m}+R_tot_JRPA_stud219x{r_m}+...
        R_tot_JRPA_stud320b{r_m}+R_tot_JRPA_stud320e{r_m}+...
        R_tot_JRPA_stud320g{r_m}+R_tot_JRPA_stud320i{r_m}+...
        R_tot_JRPA_stud320j{r_m}+R_tot_JRPA_stud320n{r_m})/10;
    R_tot_FRPA{r_m}=(R_tot_FRPA_stud219b{r_m}+R_tot_FRPA_stud219q{r_m}+...
        R_tot_FRPA_stud219t{r_m}+R_tot_FRPA_stud219x{r_m}+...
        R_tot_FRPA_stud320b{r_m}+R_tot_FRPA_stud320e{r_m}+...
        R_tot_FRPA_stud320g{r_m}+R_tot_FRPA_stud320i{r_m}+...
        R_tot_FRPA_stud320j{r_m}+R_tot_FRPA_stud320n{r_m})/10;
end

Fig_Rtot_minrate_3user=figure;
hold on
plot(rate_min_f,R_tot_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_JRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_JRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_FRPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,R_tot_FRPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,R_tot_FRPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('JSPA-Opt, R^{min}_m=0.5 bps/Hz','JSPA-Opt, R^{min}_m=1 bps/Hz','JSPA-Opt, R^{min}_m=2 bps/Hz',...
       'JRPA-CNR, R^{min}_m=0.5 bps/Hz','JRPA-CNR, R^{min}_m=1 bps/Hz','JRPA-CNR, R^{min}_m=2 bps/Hz',...
       'FRPA-CNR, R^{min}_m=0.5 bps/Hz','FRPA-CNR, R^{min}_m=1 bps/Hz','FRPA-CNR, R^{min}_m=2 bps/Hz',...
       'location','northeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total spectral efficiency (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_Rtot_minrate_3user,'Fig_Rtot_minrate_3user')

%% Sum-Rate Maximization Problem: Centralized, Semi-Centralized, Distributed Frameworks
%%% Order of NOMA clusters: Matrix(clstr_m,cls_f,r_m,r_f,sampl)
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud219b','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud219b{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud219b{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud219b{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud219q','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud219q{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud219q{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud219q{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud219t','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud219t{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud219t{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud219t{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud219x','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud219x{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud219x{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud219x{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320b','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320b{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud320b{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud320b{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320e','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320e{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud320e{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud320e{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320g','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320g{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud320g{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud320g{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320i','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320i{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud320i{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud320i{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320j','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320j{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud320j{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud320j{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_2_Alg_SumRate_usernum_stud320n','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA_stud320n{cls_m}=reshape(sum(R_tot_JSPA_SM_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_SC_stud320n{cls_m}=reshape(sum(Rtot_SC_sumrate_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
    R_tot_DF_stud320n{cls_m}=reshape(sum(R_DF_sample(cls_m,:,1,1,:),5),...
        length(num_Fcell_user),1,1,1,1)./max_samp;
end

%Merging PCs results
for cls_m=1:length(num_Mcell_user)
    R_tot_JSPA{cls_m}=(R_tot_JSPA_stud219b{cls_m}+R_tot_JSPA_stud219q{cls_m}+...
        R_tot_JSPA_stud219t{cls_m}+R_tot_JSPA_stud219x{cls_m}+...
        R_tot_JSPA_stud320b{cls_m}+R_tot_JSPA_stud320e{cls_m}+...
        R_tot_JSPA_stud320g{cls_m}+R_tot_JSPA_stud320i{cls_m}+...
        R_tot_JSPA_stud320j{cls_m}+R_tot_JSPA_stud320n{cls_m})/10;
    R_tot_SC{cls_m}=(R_tot_SC_stud219b{cls_m}+R_tot_SC_stud219q{cls_m}+...
        R_tot_SC_stud219t{cls_m}+R_tot_SC_stud219x{cls_m}+...
        R_tot_SC_stud320b{cls_m}+R_tot_SC_stud320e{cls_m}+...
        R_tot_SC_stud320g{cls_m}+R_tot_SC_stud320i{cls_m}+...
        R_tot_SC_stud320j{cls_m}+R_tot_SC_stud320n{cls_m})/10;
    R_tot_DF{cls_m}=(R_tot_DF_stud219b{cls_m}+R_tot_DF_stud219q{cls_m}+...
        R_tot_DF_stud219t{cls_m}+R_tot_DF_stud219x{cls_m}+...
        R_tot_DF_stud320b{cls_m}+R_tot_DF_stud320e{cls_m}+...
        R_tot_DF_stud320g{cls_m}+R_tot_DF_stud320i{cls_m}+...
        R_tot_DF_stud320j{cls_m}+R_tot_DF_stud320n{cls_m})/10;
end

Fig_Rtot_FW_usernum=figure;
hold on
plot(num_Fcell_user,R_tot_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,R_tot_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,R_tot_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,R_tot_SC{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,R_tot_SC{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,R_tot_SC{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,R_tot_DF{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(num_Fcell_user,R_tot_DF{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(num_Fcell_user,R_tot_DF{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('Centralized, |U_m|=2','Centralized, |U_m|=3','Centralized, |U_m|=4',...
       'Semi-Centralized, |U_m|=2','Semi-Centralized, |U_m|=3','Semi-Centralized, |U_m|=4',...
       'Distributed, |U_m|=2','Distributed, |U_m|=3','Distributed, |U_m|=4',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('|U_f|','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total spectral efficiency (bps/Hz)','FontSize',14,...
    'FontName','Times New Roman')
set(gca,'XTick',num_Fcell_user,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_Rtot_FW_usernum,'Fig_Rtot_FW_usernum')

%%Minimim Rate: Matrix(clstr_m,cls_f,r_m,r_f,sampl)

%%%M==2 for each cell
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219b','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219b{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud219b{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud219b{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219q','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219q{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud219q{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud219q{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219t','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219t{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud219t{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud219t{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219x','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219x{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud219x{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud219x{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320b','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320b{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320b{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320b{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320e','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320e{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320e{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320e{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320g','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320g{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320g{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320g{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320i','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320i{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320i{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320i{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320j','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320j{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320j{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320j{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320n','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320n{r_m}=reshape(sum(R_tot_JSPA_SM_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320n{r_m}=reshape(sum(Rtot_SC_sumrate_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320n{r_m}=reshape(sum(R_DF_sample(1,1,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    R_tot_JSPA{r_m}=(R_tot_JSPA_stud219b{r_m}+R_tot_JSPA_stud219q{r_m}+...
        R_tot_JSPA_stud219t{r_m}+R_tot_JSPA_stud219x{r_m}+...
        R_tot_JSPA_stud320b{r_m}+R_tot_JSPA_stud320e{r_m}+...
        R_tot_JSPA_stud320g{r_m}+R_tot_JSPA_stud320i{r_m}+...
        R_tot_JSPA_stud320j{r_m}+R_tot_JSPA_stud320n{r_m})/10;
    R_tot_SC{r_m}=(R_tot_SC_stud219b{r_m}+R_tot_SC_stud219q{r_m}+...
        R_tot_SC_stud219t{r_m}+R_tot_SC_stud219x{r_m}+...
        R_tot_SC_stud320b{r_m}+R_tot_SC_stud320e{r_m}+...
        R_tot_SC_stud320g{r_m}+R_tot_SC_stud320i{r_m}+...
        R_tot_SC_stud320j{r_m}+R_tot_SC_stud320n{r_m})/10;
    R_tot_DF{r_m}=(R_tot_DF_stud219b{r_m}+R_tot_DF_stud219q{r_m}+...
        R_tot_DF_stud219t{r_m}+R_tot_DF_stud219x{r_m}+...
        R_tot_DF_stud320b{r_m}+R_tot_DF_stud320e{r_m}+...
        R_tot_DF_stud320g{r_m}+R_tot_DF_stud320i{r_m}+...
        R_tot_DF_stud320j{r_m}+R_tot_DF_stud320n{r_m})/10;
end

Fig_Rtot_FW_minrate_2user=figure;
hold on
plot(rate_min_f,R_tot_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_SC{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_SC{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_SC{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_DF{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,R_tot_DF{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,R_tot_DF{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('Centralized, R^{min}_m=0.5 bps/Hz','Centralized, R^{min}_m=1 bps/Hz','Centralized, R^{min}_m=2 bps/Hz',...
       'Semi-Centralized, R^{min}_m=0.5 bps/Hz','Semi-Centralized, R^{min}_m=1 bps/Hz','Semi-Centralized, R^{min}_m=2 bps/Hz',...
       'Distributed, R^{min}_m=0.5 bps/Hz','Distributed, R^{min}_m=1 bps/Hz','Distributed, R^{min}_m=2 bps/Hz',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total spectral efficiency (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_Rtot_FW_minrate_2user,'Fig_Rtot_FW_minrate_2user')

%%%M==3 for each cell
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219b','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219b{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud219b{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud219b{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219q','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219q{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud219q{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud219q{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219t','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219t{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud219t{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud219t{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219x','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud219x{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud219x{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud219x{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320b','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320b{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320b{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320b{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320e','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320e{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320e{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320e{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320g','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320g{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320g{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320g{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320i','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320i{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320i{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320i{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320j','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320j{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320j{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320j{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320n','rate_min_m','rate_min_f','R_tot_JSPA_SM_sample',...
    'R_DF_sample','Rtot_SC_sumrate_sample')
for r_m=1:length(rate_min_m)
    R_tot_JSPA_stud320n{r_m}=reshape(sum(R_tot_JSPA_SM_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_SC_stud320n{r_m}=reshape(sum(Rtot_SC_sumrate_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
    R_tot_DF_stud320n{r_m}=reshape(sum(R_DF_sample(2,2,r_m,:,:),5),...
        length(rate_min_f),1,1,1,1)./max_samp;
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    R_tot_JSPA{r_m}=(R_tot_JSPA_stud219b{r_m}+R_tot_JSPA_stud219q{r_m}+...
        R_tot_JSPA_stud219t{r_m}+R_tot_JSPA_stud219x{r_m}+...
        R_tot_JSPA_stud320b{r_m}+R_tot_JSPA_stud320e{r_m}+...
        R_tot_JSPA_stud320g{r_m}+R_tot_JSPA_stud320i{r_m}+...
        R_tot_JSPA_stud320j{r_m}+R_tot_JSPA_stud320n{r_m})/10;
    R_tot_SC{r_m}=(R_tot_SC_stud219b{r_m}+R_tot_SC_stud219q{r_m}+...
        R_tot_SC_stud219t{r_m}+R_tot_SC_stud219x{r_m}+...
        R_tot_SC_stud320b{r_m}+R_tot_SC_stud320e{r_m}+...
        R_tot_SC_stud320g{r_m}+R_tot_SC_stud320i{r_m}+...
        R_tot_SC_stud320j{r_m}+R_tot_SC_stud320n{r_m})/10;
    R_tot_DF{r_m}=(R_tot_DF_stud219b{r_m}+R_tot_DF_stud219q{r_m}+...
        R_tot_DF_stud219t{r_m}+R_tot_DF_stud219x{r_m}+...
        R_tot_DF_stud320b{r_m}+R_tot_DF_stud320e{r_m}+...
        R_tot_DF_stud320g{r_m}+R_tot_DF_stud320i{r_m}+...
        R_tot_DF_stud320j{r_m}+R_tot_DF_stud320n{r_m})/10;
end

Fig_Rtot_FW_minrate_3user=figure;
hold on
plot(rate_min_f,R_tot_JSPA{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JSPA{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_JSPA{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,R_tot_SC{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_SC{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_SC{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,R_tot_DF{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,R_tot_DF{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','r','linestyle',':');
plot(rate_min_f,R_tot_DF{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','r','linestyle',':');
legend('Centralized, R^{min}_m=0.5 bps/Hz','Centralized, R^{min}_m=1 bps/Hz','Centralized, R^{min}_m=2 bps/Hz',...
       'Semi-Centralized, R^{min}_m=0.5 bps/Hz','Semi-Centralized, R^{min}_m=1 bps/Hz','Semi-Centralized, R^{min}_m=2 bps/Hz',...
       'Distributed, R^{min}_m=0.5 bps/Hz','Distributed, R^{min}_m=1 bps/Hz','Distributed, R^{min}_m=2 bps/Hz',...
       'location','southeast','NumColumns',2);
set(legend,'FontSize',8,'FontName','Times New Roman','EdgeColor',[0 0 0])
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total spectral efficiency (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_Rtot_FW_minrate_3user,'Fig_Rtot_FW_minrate_3user')

%% Sum-Rate Maximization Problem: BSs Power Coefficient in Centralized, and Semi-Centralized FWs
%%% Order of NOMA Clusters: Matrix(clstr_m,cls_f,r_m,r_f,sampl)
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219b','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud219b','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud219b{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud219b{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud219b{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219q','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud219q','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud219q{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud219q{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud219q{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219t','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud219t','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud219t{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud219t{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud219t{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud219x','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud219x','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud219x{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud219x{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud219x{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320b','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320b','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud320b{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud320b{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud320b{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320e','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320e','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud320e{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud320e{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud320e{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320g','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320g','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud320g{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud320g{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud320g{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320i','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320i','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud320i{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud320i{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud320i{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320j','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320j','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud320j{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud320j{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud320j{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','num_Mcell_user','num_Fcell_user','max_samp')
load('STEP2_1_Alg_Powmin_usernum_stud320n','Feasible_JSPA_sample')
load('STEP2_2_Alg_SumRate_usernum_stud320n','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for cls_m=1:length(num_Mcell_user)
    for cls_f=1:length(num_Fcell_user)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(cls_m,cls_f,1,1,smpl)*NonEmpty_JSPA_SM_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_cent_MBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(1);
                alpha_cent_FBS(cls_m,cls_f,smpl)=alpha_JSPA_SM_sample{cls_m,cls_f,1,1,smpl}(2);
                nnz_cent_MBS(cls_m,cls_f,smpl)=1;
                nnz_cent_FBS(cls_m,cls_f,smpl)=1;
            else alpha_cent_MBS(cls_m,cls_f,smpl)=0;
                alpha_cent_FBS(cls_m,cls_f,smpl)=0;
                nnz_cent_MBS(cls_m,cls_f,smpl)=0;
                nnz_cent_FBS(cls_m,cls_f,smpl)=0;
            end
            if Feasible_SC_sample(cls_m,cls_f,1,1,smpl)==1
                alpha_SC_MBS(cls_m,cls_f,smpl)=alpha_SC_sumrate_sample{cls_m,cls_f,1,1,smpl}(1);
                nnz_SC_MBS(cls_m,cls_f,smpl)=1;
            else alpha_SC_MBS(cls_m,cls_f,smpl)=0;
                nnz_SC_MBS(cls_m,cls_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(cls_m,cls_f)=sum(alpha_cent_MBS(cls_m,cls_f,:))/sum(nnz_cent_MBS(cls_m,cls_f,:));
        alpha_ave_cent_FBS(cls_m,cls_f)=sum(alpha_cent_FBS(cls_m,cls_f,:))/sum(nnz_cent_FBS(cls_m,cls_f,:));
        alpha_ave_SC_MBS(cls_m,cls_f)=sum(alpha_SC_MBS(cls_m,cls_f,:))/sum(nnz_SC_MBS(cls_m,cls_f,:));
    end
end
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS_stud320n{cls_m}=reshape(alpha_ave_cent_MBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_cent_FBS_stud320n{cls_m}=reshape(alpha_ave_cent_FBS(cls_m,:),1,length(num_Fcell_user));
    alpha_cls_SC_MBS_stud320n{cls_m}=reshape(alpha_ave_SC_MBS(cls_m,:),1,length(num_Fcell_user));
end

%Merging PCs results
for cls_m=1:length(num_Mcell_user)
    alpha_cls_cent_MBS{cls_m}=(alpha_cls_cent_MBS_stud219b{cls_m}+alpha_cls_cent_MBS_stud219q{cls_m}...
        +alpha_cls_cent_MBS_stud219t{cls_m}+alpha_cls_cent_MBS_stud219x{cls_m}+...
        alpha_cls_cent_MBS_stud320b{cls_m}+alpha_cls_cent_MBS_stud320e{cls_m}+...
        alpha_cls_cent_MBS_stud320g{cls_m}+alpha_cls_cent_MBS_stud320i{cls_m}+...
        alpha_cls_cent_MBS_stud320j{cls_m}+alpha_cls_cent_MBS_stud320n{cls_m})/10;
    alpha_cls_cent_FBS{cls_m}=(alpha_cls_cent_FBS_stud219b{cls_m}+alpha_cls_cent_FBS_stud219q{cls_m}...
        +alpha_cls_cent_FBS_stud219t{cls_m}+alpha_cls_cent_FBS_stud219x{cls_m}+...
        alpha_cls_cent_FBS_stud320b{cls_m}+alpha_cls_cent_FBS_stud320e{cls_m}+...
        alpha_cls_cent_FBS_stud320g{cls_m}+alpha_cls_cent_FBS_stud320i{cls_m}+...
        alpha_cls_cent_FBS_stud320j{cls_m}+alpha_cls_cent_FBS_stud320n{cls_m})/10;
    alpha_cls_SC_MBS{cls_m}=(alpha_cls_SC_MBS_stud219b{cls_m}+alpha_cls_SC_MBS_stud219q{cls_m}...
        +alpha_cls_SC_MBS_stud219t{cls_m}+alpha_cls_SC_MBS_stud219x{cls_m}+...
        alpha_cls_SC_MBS_stud320b{cls_m}+alpha_cls_SC_MBS_stud320e{cls_m}+...
        alpha_cls_SC_MBS_stud320g{cls_m}+alpha_cls_SC_MBS_stud320i{cls_m}+...
        alpha_cls_SC_MBS_stud320j{cls_m}+alpha_cls_SC_MBS_stud320n{cls_m})/10;
end
%MBS
Fig_alphaMBS_FW_usernum=figure;
hold on
plot(num_Fcell_user,alpha_cls_cent_MBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,alpha_cls_cent_MBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,alpha_cls_cent_MBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,alpha_cls_SC_MBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,alpha_cls_SC_MBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(num_Fcell_user,alpha_cls_SC_MBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
legend('Centralized, |U_m|=2','Centralized, |U_m|=3','Centralized, |U_m|=4',...
       'Semi-Centralized, |U_m|=2','Semi-Centralized, |U_m|=3','Semi-Centralized, |U_m|=4',...
       'location','northeast','NumColumns',1);
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('|U_f|','FontSize',16,...
    'FontName','Times New Roman')
ylabel('\alpha_m','FontSize',14,...
    'FontName','Times New Roman')
set(gca,'XTick',num_Fcell_user,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_alphaMBS_FW_usernum,'Fig_alphaMBS_FW_usernum')

%FBS
Fig_alphaFBS_FW_usernum=figure;
hold on
plot(num_Fcell_user,alpha_cls_cent_FBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,alpha_cls_cent_FBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(num_Fcell_user,alpha_cls_cent_FBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
legend('Centralized, |U_m|=2','Centralized, |U_m|=3','Centralized, |U_m|=4',...
       'location','best','NumColumns',1);
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('|U_f|','FontSize',16,...
    'FontName','Times New Roman')
ylabel('\alpha_f','FontSize',14,...
    'FontName','Times New Roman')
set(gca,'XTick',num_Fcell_user,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_alphaFBS_FW_usernum,'Fig_alphaFBS_FW_usernum')

%%Minimim Rate: Matrix(clstr_m,cls_f,r_m,r_f,sampl)

%%%M==2 for each cell
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219b','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219b','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud219b{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud219b{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud219b{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219q','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219q','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud219q{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud219q{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud219q{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219t','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219t','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud219t{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud219t{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud219t{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud219x','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud219x','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud219x{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud219x{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud219x{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320b','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320b','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320b{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320b{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320b{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320e','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320e','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320e{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320e{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320e{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320g','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320g','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320g{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320g{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320g{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320i','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320i','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320i{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320i{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320i{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320j','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320j','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320j{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320j{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320j{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320n
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_1_Alg_Powmin_2user_minrate_stud320n','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_2user_minrate_stud320n','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(1,1,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(1,1,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{1,1,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(1,1,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{1,1,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320n{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320n{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320n{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS{r_m}=(alpha_cls_cent_MBS_stud219b{r_m}+alpha_cls_cent_MBS_stud219q{r_m}...
        +alpha_cls_cent_MBS_stud219t{r_m}+alpha_cls_cent_MBS_stud219x{r_m}+...
        alpha_cls_cent_MBS_stud320b{r_m}+alpha_cls_cent_MBS_stud320e{r_m}+...
        alpha_cls_cent_MBS_stud320g{r_m}+alpha_cls_cent_MBS_stud320i{r_m}+...
        alpha_cls_cent_MBS_stud320j{r_m}+alpha_cls_cent_MBS_stud320n{r_m})/10;
    alpha_cls_cent_FBS{r_m}=(alpha_cls_cent_FBS_stud219b{r_m}+alpha_cls_cent_FBS_stud219q{r_m}...
        +alpha_cls_cent_FBS_stud219t{r_m}+alpha_cls_cent_FBS_stud219x{r_m}+...
        alpha_cls_cent_FBS_stud320b{r_m}+alpha_cls_cent_FBS_stud320e{r_m}+...
        alpha_cls_cent_FBS_stud320g{r_m}+alpha_cls_cent_FBS_stud320i{r_m}+...
        alpha_cls_cent_FBS_stud320j{r_m}+alpha_cls_cent_FBS_stud320n{r_m})/10;
    alpha_cls_SC_MBS{r_m}=(alpha_cls_SC_MBS_stud219b{r_m}+alpha_cls_SC_MBS_stud219q{r_m}+...
        alpha_cls_SC_MBS_stud219t{r_m}+alpha_cls_SC_MBS_stud219x{r_m}+...
        alpha_cls_SC_MBS_stud320b{r_m}+alpha_cls_SC_MBS_stud320e{r_m}+...
        alpha_cls_SC_MBS_stud320g{r_m}+alpha_cls_SC_MBS_stud320i{r_m}+...
        alpha_cls_SC_MBS_stud320j{r_m}+alpha_cls_SC_MBS_stud320n{r_m})/10;
end
%MBS
Fig_alphaMBS_FW_minrate_2user=figure;
hold on
plot(rate_min_f,alpha_cls_cent_MBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_cent_MBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_cent_MBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_SC_MBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,alpha_cls_SC_MBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,alpha_cls_SC_MBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
legend('Centralized, R^{min}_m=0.5 bps/Hz','Centralized, R^{min}_m=1 bps/Hz','Centralized, R^{min}_m=2 bps/Hz',...
       'Semi-Centralized, R^{min}_m=0.5 bps/Hz','Semi-Centralized, R^{min}_m=1 bps/Hz','Semi-Centralized, R^{min}_m=2 bps/Hz',...
       'location','best','NumColumns',1);
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('\alpha_m','FontSize',14,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_alphaMBS_FW_minrate_2user,'Fig_alphaMBS_FW_minrate_2user')

%FBS
Fig_alphaFBS_FW_minrate_2user=figure;
hold on
plot(rate_min_f,alpha_cls_cent_FBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_cent_FBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_cent_FBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
legend('Centralized, R^{min}_m=0.5 bps/Hz','Centralized, R^{min}_m=1 bps/Hz','Centralized, R^{min}_m=2 bps/Hz',...
       'location','best','NumColumns',1);
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('\alpha_f','FontSize',14,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_alphaFBS_FW_minrate_2user,'Fig_alphaFBS_FW_minrate_2user')

%%%M==3 for each cell
clear all
clc

%PC stud219b
load('STEP1_Settings_MultiCell_stud219b','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219b','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219b','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud219b{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud219b{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud219b{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud219q
load('STEP1_Settings_MultiCell_stud219q','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219q','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219q','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud219q{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud219q{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud219q{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud219t
load('STEP1_Settings_MultiCell_stud219t','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219t','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219t','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud219t{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud219t{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud219t{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud219x
load('STEP1_Settings_MultiCell_stud219x','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud219x','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud219x','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud219x{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud219x{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud219x{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320b
load('STEP1_Settings_MultiCell_stud320b','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320b','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320b','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320b{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320b{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320b{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320e
load('STEP1_Settings_MultiCell_stud320e','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320e','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320e','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320e{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320e{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320e{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320g
load('STEP1_Settings_MultiCell_stud320g','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320g','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320g','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320g{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320g{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320g{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320i
load('STEP1_Settings_MultiCell_stud320i','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320i','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320i','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320i{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320i{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320i{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320j','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320j','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320j','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320j{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320j{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320j{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end
%PC stud320j
load('STEP1_Settings_MultiCell_stud320n','max_samp')
load('STEP2_1_Alg_Powmin_3user_minrate_stud320n','Feasible_JSPA_sample','rate_min_m','rate_min_f')
load('STEP2_2_Alg_SumRate_3user_minrate_stud320n','alpha_JSPA_SM_sample',...
    'alpha_SC_sumrate_sample','Feasible_SC_sample','NonEmpty_JSPA_SM_sample')
for r_m=1:length(rate_min_m)
    for r_f=1:length(rate_min_f)
        for smpl=1:max_samp
            if Feasible_JSPA_sample(2,2,r_m,r_f,smpl)*NonEmpty_JSPA_SM_sample(2,2,r_m,r_f,smpl)==1
                alpha_cent_MBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(1);
                alpha_cent_FBS(r_m,r_f,smpl)=alpha_JSPA_SM_sample{2,2,r_m,r_f,smpl}(2);
                nnz_cent_MBS(r_m,r_f,smpl)=1;
                nnz_cent_FBS(r_m,r_f,smpl)=1;
            else alpha_cent_MBS(r_m,r_f,smpl)=0;
                alpha_cent_FBS(r_m,r_f,smpl)=0;
                nnz_cent_MBS(r_m,r_f,smpl)=0;
                nnz_cent_FBS(r_m,r_f,smpl)=0;
            end
            if Feasible_SC_sample(2,2,r_m,r_f,smpl)==1
                alpha_SC_MBS(r_m,r_f,smpl)=alpha_SC_sumrate_sample{2,2,r_m,r_f,smpl}(1);
                nnz_SC_MBS(r_m,r_f,smpl)=1;
            else alpha_SC_MBS(r_m,r_f,smpl)=0;
                nnz_SC_MBS(r_m,r_f,smpl)=0;
            end
        end
        alpha_ave_cent_MBS(r_m,r_f)=sum(alpha_cent_MBS(r_m,r_f,:))/sum(nnz_cent_MBS(r_m,r_f,:));
        alpha_ave_cent_FBS(r_m,r_f)=sum(alpha_cent_FBS(r_m,r_f,:))/sum(nnz_cent_FBS(r_m,r_f,:));
        alpha_ave_SC_MBS(r_m,r_f)=sum(alpha_SC_MBS(r_m,r_f,:))/sum(nnz_SC_MBS(r_m,r_f,:));
    end
end
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS_stud320n{r_m}=reshape(alpha_ave_cent_MBS(r_m,:),1,length(rate_min_f));
    alpha_cls_cent_FBS_stud320n{r_m}=reshape(alpha_ave_cent_FBS(r_m,:),1,length(rate_min_f));
    alpha_cls_SC_MBS_stud320n{r_m}=reshape(alpha_ave_SC_MBS(r_m,:),1,length(rate_min_f));
end

%Merging PCs results
for r_m=1:length(rate_min_m)
    alpha_cls_cent_MBS{r_m}=(alpha_cls_cent_MBS_stud219b{r_m}+alpha_cls_cent_MBS_stud219q{r_m}...
        +alpha_cls_cent_MBS_stud219t{r_m}+alpha_cls_cent_MBS_stud219x{r_m}+...
        alpha_cls_cent_MBS_stud320b{r_m}+alpha_cls_cent_MBS_stud320e{r_m}+...
        alpha_cls_cent_MBS_stud320g{r_m}+alpha_cls_cent_MBS_stud320i{r_m}+...
        alpha_cls_cent_MBS_stud320j{r_m}+alpha_cls_cent_MBS_stud320n{r_m})/10;
    alpha_cls_cent_FBS{r_m}=(alpha_cls_cent_FBS_stud219b{r_m}+alpha_cls_cent_FBS_stud219q{r_m}...
        +alpha_cls_cent_FBS_stud219t{r_m}+alpha_cls_cent_FBS_stud219x{r_m}+...
        alpha_cls_cent_FBS_stud320b{r_m}+alpha_cls_cent_FBS_stud320e{r_m}+...
        alpha_cls_cent_FBS_stud320g{r_m}+alpha_cls_cent_FBS_stud320i{r_m}+...
        alpha_cls_cent_FBS_stud320j{r_m}+alpha_cls_cent_FBS_stud320n{r_m})/10;
    alpha_cls_SC_MBS{r_m}=(alpha_cls_SC_MBS_stud219b{r_m}+alpha_cls_SC_MBS_stud219q{r_m}+...
        alpha_cls_SC_MBS_stud219t{r_m}+alpha_cls_SC_MBS_stud219x{r_m}+...
        alpha_cls_SC_MBS_stud320b{r_m}+alpha_cls_SC_MBS_stud320e{r_m}+...
        alpha_cls_SC_MBS_stud320g{r_m}+alpha_cls_SC_MBS_stud320i{r_m}+...
        alpha_cls_SC_MBS_stud320j{r_m}+alpha_cls_SC_MBS_stud320n{r_m})/10;
end

%MBS
Fig_alphaMBS_FW_minrate_3user=figure;
hold on
plot(rate_min_f,alpha_cls_cent_MBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_cent_MBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_cent_MBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_SC_MBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,alpha_cls_SC_MBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','b','linestyle','--');
plot(rate_min_f,alpha_cls_SC_MBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','b','linestyle','--');
legend('Centralized, R^{min}_m=0.5 bps/Hz','Centralized, R^{min}_m=1 bps/Hz','Centralized, R^{min}_m=2 bps/Hz',...
       'Semi-Centralized, R^{min}_m=0.5 bps/Hz','Semi-Centralized, R^{min}_m=1 bps/Hz','Semi-Centralized, R^{min}_m=2 bps/Hz',...
       'location','best','NumColumns',1);
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('\alpha_m','FontSize',14,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_alphaMBS_FW_minrate_3user,'Fig_alphaMBS_FW_minrate_3user')

%FBS
Fig_alphaFBS_FW_minrate_3user=figure;
hold on
plot(rate_min_f,alpha_cls_cent_FBS{1},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','o','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_cent_FBS{2},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','>','linewidth',2,'Color','k','linestyle','-');
plot(rate_min_f,alpha_cls_cent_FBS{3},'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',8,'Marker','h','linewidth',2,'Color','k','linestyle','-');
legend('Centralized, R^{min}_m=0.5 bps/Hz','Centralized, R^{min}_m=1 bps/Hz','Centralized, R^{min}_m=2 bps/Hz',...
       'location','best','NumColumns',1);
set(legend,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('R^{min}_f (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
ylabel('\alpha_f','FontSize',14,...
    'FontName','Times New Roman')
set(gca,'XTick',rate_min_f,'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_alphaFBS_FW_minrate_3user,'Fig_alphaFBS_FW_minrate_3user')

%% Opening Figures
set(0,'DefaultFigureVisible','on');

%%%%Outage Probability: JSPA, JRPA, FRPA
%Order of NOMA Cluster:
openfig('Fig_outage_Algs_usernum','visible');
%Minimim Rate: 2 Users
openfig('Fig_outage_Algs_minrate_2user','visible');
%Minimim Rate: 3 Users
openfig('Fig_outage_Algs_minrate_3user','visible');

%%%%Outage Probability: Centralized, Semi-Centralized, Distributed Frameworks
%Order of NOMA Cluster:
openfig('Fig_outage_FW_usernum','visible');
%Minimim Rate: 2 Users
openfig('Fig_outage_FW_minrate_2user','visible');
%Minimim Rate: 3 Users
openfig('Fig_outage_FW_minrate_3user','visible');

% %%%%Prower Minimization Problem: Total Power Consumption of JSPA, JRPA, FRPA
% %Order of NOMA Cluster:
% openfig('fig_totPowermin_usernum','visible');
% %Minimim Rate: 2 Users
% openfig('fig_totpowermin_minrate_2user','visible');
% %Minimim Rate: 3 Users
% openfig('fig_totpowermin_minrate_3user','visible');

%%%%Sum-Rate Maximization Problem: Total Spectral Efficiency of JSPA, JRPA, FRPA
%Order of NOMA Cluster:
openfig('Fig_Rtot_usernum','visible');
%Minimim Rate: 2 Users
openfig('Fig_Rtot_minrate_2user','visible');
%Minimim Rate: 3 Users
openfig('Fig_Rtot_minrate_3user','visible');

%%%%Sum-Rate Maximization Problem: Centralized, Semi-Centralized, Distributed Frameworks
%Order of NOMA Cluster:
openfig('Fig_Rtot_FW_usernum','visible');
%Minimim Rate: 2 Users
openfig('Fig_Rtot_FW_minrate_2user','visible');
%Minimim Rate: 3 Users
openfig('Fig_Rtot_FW_minrate_3user','visible');

%%%%Sum-Rate Maximization Problem: BSs Power Coefficient in Centralized, and Semi-Centralized FWs
%%Order of NOMA Cluster:
%MBS
openfig('Fig_alphaMBS_FW_usernum','visible');
%FBS
openfig('Fig_alphaFBS_FW_usernum','visible');
%%Minimim Rate: 2 Users
%MBS
openfig('Fig_alphaMBS_FW_minrate_2user','visible');
%FBS
openfig('Fig_alphaFBS_FW_minrate_2user','visible');
%%Minimim Rate: 3 Users
%MBS
openfig('Fig_alphaMBS_FW_minrate_3user','visible');
%FBS
openfig('Fig_alphaFBS_FW_minrate_3user','visible');