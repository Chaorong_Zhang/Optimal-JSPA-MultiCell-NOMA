%% Macro-cell
clear all
clc
num_Mcell_user=[2:6]; %number of macro-cell users
num_Fcell_user=[1]; %number of femto-cell users
Distance_BS=[200]; %distance between macro-cell and femto-cell
BW=5e+6; %wireless bandwidth
PSD_noise=[-174]; %PSD of AWGN in dBm/Hz
max_samp=20000; %number of samples
fprintf('***Macro-cell Total Samples***\n\n')
disp(length(num_Mcell_user)*length(num_Fcell_user)*length(Distance_BS)...
    *length(PSD_noise)*max_samp)
for clstr_m=1:length(num_Mcell_user)
    for clstr_f=1:length(num_Fcell_user)
        for dist=1:length(Distance_BS)
            for N0_samp=1:length(PSD_noise)
                for sampl=1:max_samp 
            %% Simulation Settings
            %%%%%Network Topology
            Dis_BS=Distance_BS(dist);
            Cor_BS=func_CoordinateBS(Dis_BS);
            B=numel(Cor_BS); %Number of BSs
            radius_BS=[500,40*ones(1,B-1)]; %radius of each BS in meter
            Macro_cell=num_Mcell_user(clstr_m);
            Femto_cell=num_Fcell_user(clstr_f);
            U=[Macro_cell,Femto_cell*ones(1,B-1)];
            M=max(U);
            min_Dis_User=[10,2*ones(1,B-1)]; %minimum distance of user to associated BS in meter
            %%%%%Coordinate of users
            Cor_user=func_CoordinateUser(B,Cor_BS,U,M,radius_BS,min_Dis_User);
            [h,h_cell]=func_ChannelGain(B,U,M,Cor_BS,Cor_user);
            h_sample{clstr_m,clstr_f,dist,N0_samp,sampl}=h; %save sample channel gains
            h_cell_sample{clstr_m,clstr_f,dist,N0_samp,sampl}=h_cell; %save sample channel gains
            %Noise power
            mean_N0=PSD_noise(N0_samp); %PSD of Noise in dBm/Hz
            mean_N0=10.^((mean_N0-30)/10); %PSD of Noise in watts/Hz
            N0=abs(randn(B,M)).*mean_N0*BW; %AWGN power at users
            AWGN_sample{clstr_m,clstr_f,dist,N0_samp,sampl}=N0; %save sample AWGN power at users
            
            %% Sufficient Condition Examination
            %%%%%Transmit power of BSs
            P_MBS=46; %MBS power in dbm
            P_FBS=30; %MBS power in dbm
            P_max=[P_MBS,P_FBS*ones(1,B-1)]; %BSs power (in dbm)
            P_max=10.^((P_max-30)./10); %BSs power in Watts
            [perc_pair_unsatisfy,num_pairs_unsatisfy]=func_suff_condition_optSIC(B,U,M,h,h_cell,P_max,N0);
            num_pairs_unsatisfy_MBS_sample(clstr_m,sampl)=num_pairs_unsatisfy(1);
            perc_pair_unsatisfy_MBS_sample(clstr_m,sampl)=perc_pair_unsatisfy(1);
                end
            end
        end
    end
end
save('Result_Step1_SufficientCondition_MBS','num_Mcell_user','max_samp','num_pairs_unsatisfy_MBS_sample',...
    'perc_pair_unsatisfy_MBS_sample')

%% Femto-cell
clear all
num_Mcell_user=[1]; %number of macro-cell users
num_Fcell_user=[2:6]; %number of femto-cell users
Distance_BS=[25,50,100,300]; %distance between macro-cell and femto-cell
BW=5e+6; %wireless bandwidth
PSD_noise=[-174]; %PSD of AWGN in dBm/Hz
max_samp=20000; %number of samples
fprintf('***Femto-cell Total Samples***\n\n')
disp(length(num_Mcell_user)*length(num_Fcell_user)*length(Distance_BS)...
    *length(PSD_noise)*max_samp)
for clstr_m=1:length(num_Mcell_user)
    for clstr_f=1:length(num_Fcell_user)
        for dist=1:length(Distance_BS)
            for N0_samp=1:length(PSD_noise)
                for sampl=1:max_samp 
            %% Simulation Settings
            %%%%%Network Topology
            Dis_BS=Distance_BS(dist);
            Cor_BS=func_CoordinateBS(Dis_BS);
            B=numel(Cor_BS); %Number of BSs
            radius_BS=[500,40*ones(1,B-1)]; %radius of each BS in meter
            Macro_cell=num_Mcell_user(clstr_m);
            Femto_cell=num_Fcell_user(clstr_f);
            U=[Macro_cell,Femto_cell*ones(1,B-1)];
            M=max(U);
            min_Dis_User=[10,2*ones(1,B-1)]; %minimum distance of user to associated BS in meter
            %%%%%Coordinate of users
            Cor_user=func_CoordinateUser(B,Cor_BS,U,M,radius_BS,min_Dis_User);
            [h,h_cell]=func_ChannelGain(B,U,M,Cor_BS,Cor_user);
            h_sample{clstr_m,clstr_f,dist,N0_samp,sampl}=h; %save sample channel gains
            h_cell_sample{clstr_m,clstr_f,dist,N0_samp,sampl}=h_cell; %save sample channel gains
            %Noise power
            mean_N0=PSD_noise(N0_samp); %PSD of Noise in dBm/Hz
            mean_N0=10.^((mean_N0-30)/10); %PSD of Noise in watts/Hz
            N0=abs(randn(B,M)).*mean_N0*BW; %AWGN power at users
            AWGN_sample{clstr_m,clstr_f,dist,N0_samp,sampl}=N0; %save sample AWGN power at users
            
            %% Sufficient Condition Examination
            %%%%%Transmit power of BSs
            P_MBS=46; %MBS power in dbm
            P_FBS=30; %MBS power in dbm
            P_max=[P_MBS,P_FBS*ones(1,B-1)]; %BSs power (in dbm)
            P_max=10.^((P_max-30)./10); %BSs power in Watts
            [perc_pair_unsatisfy,num_pairs_unsatisfy]=func_suff_condition_optSIC(B,U,M,h,h_cell,P_max,N0);
            num_pairs_unsatisfy_FBS_sample(clstr_f,dist,sampl)=num_pairs_unsatisfy(2);
            perc_pair_unsatisfy_FBS_sample(clstr_f,dist,sampl)=perc_pair_unsatisfy(2);
                end
            end
        end
    end
end
disp('---Finished!---')
save('Result_Step1_SufficientCondition_FBS','num_Fcell_user','Distance_BS','max_samp','num_pairs_unsatisfy_FBS_sample',...
    'perc_pair_unsatisfy_FBS_sample')
