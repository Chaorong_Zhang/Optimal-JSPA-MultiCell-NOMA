%% Convergence of Sequential Programming > Sequential Programming...

%% Initialization Method: Equal power allocation (EPA)
clear all
clc
load('STEP1_GeneralCode_MultiCell_NOMA','B','U','M','N0','h','h_cell',...
    'ICI_indctr','P_max','R_min','SIC_CNR','lambda_CNR','Feasible_JRPA','CNR_opt','tt_max')
fprintf('***Equal power allocation (EPA)***\n\n')
if Feasible_JRPA==1
    %Initialization step
    h_norm=h_cell./(N0); %CSI based SIC
    SIC_order=SIC_CNR; %SIC ordering based on CSI
    lambda=lambda_CNR; %cancellation decision indicator based on CSI-based decoding order
    p=zeros(B,M);
    for b=1:B
        for i=1:U(b)
            p(b,i)=P_max(b)./U(b);
        end
    end
    %%Rate
    ICI_term=zeros(B,B,M,M);
    r=zeros(B,M);
    for b=1:B
        for i=1:M
            for k=1:M
                for j=1:B
                    ICI_term(j,b,i,k)=ICI_indctr(j,b,i)*p(j,i)*h(j,b,k);
                end
                ICI_vp(b,i,k)=sum(ICI_term(:,b,i,k));
                R_vp(b,i,k)=log2(1+ ( p(b,i)*h_cell(b,k) )/...
                    (sum(reshape(lambda(b,i,:),1,M).* p(b,:))*h_cell(b,k) + ...
                    ICI_vp(b,i,k)+N0(b,k)));
            end
            r(b,i)=R_vp(b,i,i);
            if sum(lambda(b,i,:))>0
                r(b,i)=min(r(b,i),min(nnz(reshape(lambda(b,i,:).*R_vp(b,i,:),1,M))));
            end
        end
    end
    if min(r>=R_min)==0
        fprintf('---Initialization step is infeasible!\n\n')
    end
    t=1; %iteration index of initial step
    R_user_EPA{t}=r;
    R_tot_EPA(t)=sum(r,'all');
    p_EPA{t}=p;
    fprintf('>>>>>>Sequential Programming...\n\n')
    p_tilde=log(p);
    Failure_EPA=0;
    Failure_EPA_CVX=0;
    while (t < tt_max)
        t=t+1
        r_old=r;	p_tilde_old=p_tilde;
        %CVX
        r=[];	p=[];	p_tilde=[];    ICI=[];
        cvx_begin quiet
        cvx_precision default
            variable p_tilde(B,M)
            variable r(B,M)
            subject to
            %power constraint
            sum(exp(p_tilde),2)<=P_max';
            %Min rate constraint (main)
            ICI=reshape(sum(repmat(sum(exp(p_tilde),2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
            for b=1:B
                for i=1:M
                    if i<=U(b)
                        1e2*(p_tilde(b,i) + log(h_cell(b,i))) >= 1e2*( log(2^r_old(b,i)-1) + ...
                            (2^r_old(b,i)/(2^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                            log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,i) ...
                            + ICI(b,i) + N0(b,i)));
                    else r(b,i)==0
                    end
                end
            end
            %SIC constraint
            for b=1:B
                for i=1:U(b)
                    for k=1:U(b)
                        if lambda(b,i,k)==1
                            1e2*(p_tilde(b,i) + log(h_cell(b,k))) >= 1e2*( log(2.^r_old(b,i)-1) + ...
                            ((2.^r_old(b,i))/(2.^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                            log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,k) ...
                            + ICI(b,k) + N0(b,k)));
                        end
                    end
                end
            end
            %minimum rate
            r>=R_min;
            %Positive power
            r>=0;
            maximize sum(sum(r))   
        cvx_end
        if min(isfinite(r),[],'all')==0
            if t==2
                fprintf('\n---EPA makes the next iteration infeasible!---\n\n')
                Failure_EPA=1;
                Failure_EPA_CVX=0;
            else
                fprintf('\n---CVX failed to solve the problem!---\n\n')
                Failure_EPA=0;
                Failure_EPA_CVX=1;
            end
            break
        end
        R_user_EPA{t}=r;
        R_tot_EPA(t)=sum(r,'all');
        p_EPA{t}=exp(p_tilde);
    end
    save('STEP2_Sequential_Program_EPA','R_user_EPA','R_tot_EPA','p_EPA',...
        'Failure_EPA','Failure_EPA_CVX')
end

%% Initialization Method: Approximated Rate Function (ARF)
clear all
clc
load('STEP1_GeneralCode_MultiCell_NOMA','B','U','M','N0','h','h_cell',...
    'ICI_indctr','P_max','R_min','SIC_CNR','lambda_CNR','Feasible_JRPA','CNR_opt','tt_max')
fprintf('***Approximated Rate Function (ARF)***\n\n')
if Feasible_JRPA==1
    %Initialization step
    Failure_ARF=0;
    Failure_ARF_CVX=0;
    h_norm=h_cell./(N0); %CNR-Based SIC
    SIC_order=SIC_CNR; %SIC ordering based on CNR
    lambda=lambda_CNR; %cancellation decision indicator
    m=(log(2.^30 - 1) - log(2.^(29.999) - 1))/0.001; %approx term log(2.^r - 1);
    t=1; %iteration index
    cvx_begin quiet
    cvx_precision default
        variable p_tilde(B,M)
        variable r(B,M)
        subject to
        %power constraint
        sum(exp(p_tilde),2)<=P_max';
        %Min rate constraint (main)
        ICI=reshape(sum(repmat(sum(exp(p_tilde),2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
        for b=1:B
            for i=1:M
                if i<=U(b)
                    1e2*(p_tilde(b,i) + log(h_cell(b,i))) >= 1e2*( m*r(b,i) + ...
                        log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,i) ...
                        + ICI(b,i) + N0(b,i)));
                else r(b,i)==0
                end
            end
        end    
        %SIC constraint
        for b=1:B
            for i=1:U(b)
                for k=1:U(b)
                    if lambda(b,i,k)==1
                        1e2*(p_tilde(b,i) + log(h_cell(b,k))) >= 1e2*( m*r(b,i) + ...
                            log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,k) ...
                            + ICI(b,k) + N0(b,k)));
                    end
                end
            end
        end
        %minimum rate
        r>=R_min;
        %Positive power
        r>=0;
        maximize sum(sum(r))   
    cvx_end
    if min(isfinite(r),[],'all')==0
        Failure_ARF=1;
        save('STEP2_Sequential_Program_ARF','Failure_ARF')
        fprintf('---Initialization step is <infeasible>. ARF method Failed!\n\n')
    else
        t=1; %iteration index of initial step
        R_user_ARF{t}=r;
        R_tot_ARF(t)=sum(r,'all');
        p_ARF{t}=exp(p_tilde);
        fprintf('>>>>>>Sequential Programming...\n\n')
        Failure_ARF_CVX=0;
        while (t < tt_max)
            t=t+1
            r_old=r;	p_tilde_old=p_tilde;
            %CVX
            r=[];	p=[];	p_tilde=[];    ICI=[];
            cvx_begin quiet
            cvx_precision default
                variable p_tilde(B,M)
                variable r(B,M)
                subject to
                %power constraint
                sum(exp(p_tilde),2)<=P_max';
                %Min rate constraint (main)
                ICI=reshape(sum(repmat(sum(exp(p_tilde),2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
                for b=1:B
                    for i=1:M
                        if i<=U(b)
                            1e2*(p_tilde(b,i) + log(h_cell(b,i))) >= 1e2*( log(2^r_old(b,i)-1) + ...
                                (2^r_old(b,i)/(2^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                                log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,i) ...
                                + ICI(b,i) + N0(b,i)));
                        else r(b,i)==0
                        end
                    end
                end
                %SIC constraint
                for b=1:B
                    for i=1:U(b)
                        for k=1:U(b)
                            if lambda(b,i,k)==1
                                1e2*(p_tilde(b,i) + log(h_cell(b,k))) >= 1e2*( log(2.^r_old(b,i)-1) + ...
                                ((2.^r_old(b,i))/(2.^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                                log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,k) ...
                                + ICI(b,k) + N0(b,k)));
                            end
                        end
                    end
                end
                %minimum rate
                r>=R_min;
                %Positive power
                r>=0;
                maximize sum(sum(r))   
            cvx_end
            if min(isfinite(r),[],'all')==0
                fprintf('\n---CVX failed to solve the problem!---\n\n')
                Failure_ARF_CVX=1;
                break
            end
            R_user_ARF{t}=r;
            R_tot_ARF(t)=sum(r,'all');
            p_ARF{t}=exp(p_tilde);
        end
        save('STEP2_Sequential_Program_ARF','R_user_ARF','R_tot_ARF','p_ARF',...
            'Failure_ARF','Failure_ARF_CVX')
    end
end

%% Sum-rate Maximization Problem: Minimum Rate Equality (MRE)
clear all
clc
load('STEP1_GeneralCode_MultiCell_NOMA','B','U','M','N0','h','h_cell','ICI_indctr',...
    'P_max','R_min','SIC_CNR','lambda_CNR','Feasible_JRPA','CNR_opt','tt_max','p_opt_JRPA_PM')
fprintf('***Minimum Rate Equality (MRE)***\n\n')
if Feasible_JRPA==1
    %Initialization step
    h_norm=h_cell./(N0); %CNR-Based SIC
    SIC_order=SIC_CNR; %SIC ordering based on CNR
    lambda=lambda_CNR; %cancellation decision indicator
    t=1; %iteration index of initial step
    r=R_min;
    p=p_opt_JRPA_PM;
    p_tilde=log(p);
    R_user_MRE{t}=r;
    R_tot_MRE(t)=sum(r,'all');
    p_MRE{t}=p;
    fprintf('>>>>>>Sequential Programming...\n\n')
    Failure_MRE_CVX=0;
    while (t < tt_max)
        t=t+1
        r_old=r;	p_tilde_old=p_tilde;
        %CVX
        r=[];	p=[];	p_tilde=[];    ICI=[];
        cvx_begin quiet
        cvx_precision default
            variable p_tilde(B,M)
            variable r(B,M)
            subject to
            %power constraint
            sum(exp(p_tilde),2)<=P_max';
            %Min rate constraint (main)
            ICI=reshape(sum(repmat(sum(exp(p_tilde),2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
            for b=1:B
                for i=1:M
                    if i<=U(b)
                        1e2*(p_tilde(b,i) + log(h_cell(b,i))) >= 1e2*( log(2^r_old(b,i)-1) + ...
                            (2^r_old(b,i)/(2^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                            log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,i) ...
                            + ICI(b,i) + N0(b,i)));
                    else r(b,i)==0
                    end
                end
            end
            %SIC constraint
            for b=1:B
                for i=1:U(b)
                    for k=1:U(b)
                        if lambda(b,i,k)==1
                            1e2*(p_tilde(b,i) + log(h_cell(b,k))) >= 1e2*( log(2.^r_old(b,i)-1) + ...
                            ((2.^r_old(b,i))/(2.^r_old(b,i)-1))*(r(b,i)-r_old(b,i)) + ...
                            log( sum(reshape(lambda(b,i,:),1,M).* exp(p_tilde(b,:)) ) .* h_cell(b,k) ...
                            + ICI(b,k) + N0(b,k)));
                        end
                    end
                end
            end
            %minimum rate
            r>=R_min;
            %Positive power
            r>=0;
            maximize sum(sum(r))   
        cvx_end
        if min(isfinite(r),[],'all')==0
            fprintf('\n---CVX failed to solve the problem!---\n\n')
            Failure_MRE_CVX=1;
            break
        end
        R_user_MRE{t}=r;
        R_tot_MRE(t)=sum(r,'all');
        p_MRE{t}=exp(p_tilde);
    end
    save('STEP2_Sequential_Program_MRE','R_user_MRE','R_tot_MRE','p_MRE','Failure_MRE_CVX')
end