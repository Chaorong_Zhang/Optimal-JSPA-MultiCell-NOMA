function [Feasibility_greed]=func_greedy_feasibility(B,U,M,R_min,P_max,ICI_indctr,h,h_cell,N0,alpha_free)
eps_a=1e-3;
ceil_amin=ceil(alpha_free*ceil(eps_a.^-1))./eps_a.^-1;
alpha_initial=ceil_amin;
fprintf('***Greedy Search for Verifying Infeasibility***\n\n\n')
[Feasibility_greed]=func_ExSearch_minpower(B,U,M,R_min,P_max,ICI_indctr,h,h_cell,N0,eps_a,alpha_initial);
if Feasibility_greed==1
    fprintf('<<<Iterative Algorithm is <NOT> Optimal!!!>>>\n\n\n')
else fprintf('++The infeasibility is verified by Greedy Algorithm.\n\n\n')
end