function [q]=func_approx_sumratemax(B,U,M,SIC_order,R_min)
q=zeros(B,M);
beta=(2.^R_min-1)./2.^R_min;
for b=1:B
    for i=1:U(b)
        if (SIC_order(b,i)==min(SIC_order(b,1:U(b))))
            q(b,i)=beta(b,i);
        elseif ( min(SIC_order(b,1:U(b))) < SIC_order(b,i) && SIC_order(b,i) < max(SIC_order(b,1:U(b))) )
            prod_user=find(SIC_order(b,1:U(b)) < SIC_order(b,i));
            beta_term=1;
            el=1;
            while (el <= numel(prod_user))
                beta_term=beta_term*(1-beta(b,prod_user(el)));
                el=el+1;
            end
            q(b,i)=beta(b,i)*beta_term;
        end
    end
    strg_user=find(SIC_order(b,1:U(b)) == max(SIC_order(b,1:U(b))));
    q(b,strg_user)=1-sum(q(b,:));
end
q=100*q;

