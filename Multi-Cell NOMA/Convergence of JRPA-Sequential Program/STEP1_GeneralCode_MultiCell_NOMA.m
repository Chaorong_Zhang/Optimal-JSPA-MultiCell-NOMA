%% Convergence of Sequential Programming > System Model and Optimal Solution
clear all
clc
%% Simulation Settings
%%%%%Network Topology
%BS placement
Dis_BS=200; %BS to center
Cor_BS=func_CoordinateBS(Dis_BS); %Coordinate of BSs
B=numel(Cor_BS); %Number of BSs
%User placement
radius_BS=[500,40*ones(1,B-1)]; %radius of each BS in meter
%Number of users in each cell
Macro_cell_users=3;
Femto_cell_users=2;
U=[Macro_cell_users,Femto_cell_users*ones(1,B-1)];
M=max(U);
min_dis_user=[20,2*ones(1,B-1)]; %minimum distance of user to associated BS in meter
%Noise power
BW=5e+6; %wireless bandwidth
PSD_N0=-174; %PSD of Noise in dBm
PSD_N0=10.^((PSD_N0-30)/10); %PSD of Noise in watts
N0=abs(randn(B,M)).*PSD_N0.*BW; %AWGN power at users
%%%%%Coordinate of users
Cor_user=func_CoordinateUser(B,Cor_BS,U,M,radius_BS,min_dis_user);
[h,h_cell]=func_ChannelGain(B,U,M,Cor_BS,Cor_user);
%%Interfering BS indicator for simplifying the simulation codes
ICI_indctr=func_ICI_indicator(B,M);
%%%%%Transmit power of BSs
P_MBS=46; %MBS power in dbm
P_FBS=30; %MBS power in dbm
P_max=[P_MBS,P_FBS*ones(1,B-1)]; %BSs power (in dbm)
P_max=10.^((P_max-30)./10); %BSs power in Watts
%%%%%Minimum spectral efficiency
R_macro=1;%bps/Hz
R_femto=2;%bps/Hz
[R_min]=func_minrate(B,U,M,R_macro,R_femto);
%sequential programming max iteration number
tt_max=41;

% %% Figure of Network Topology
% [Leg_coverage_BS]=func_FigTopology(B,U,M,Dis_BS,radius_BS,Cor_BS,Cor_user);
% clear Dis_BS Cor_BS radius_BS min_Dis_User Cor_user b F_cell ...
%   M_cell R_macro R_femto Femto_cell Macro_cell mean_N0 P_MBS P_FBS

%% Feasibility Check in ICI-free Scenario>> Are There Enough Powers to Maintain Rate Demands?!
[Feasible_free,SIC_CNR,lambda_CNR,alpha_free]=func_ICI_free_scenario(h_cell,N0,B,U,M,R_min,P_max);

%% Sufficient Condition Examination
fprintf('\n***SIC sufficient condition for verifying the optimality of the CNR-based decoding order\n\n\n')
[perc_pair_unsatisfy,num_pairs_unsatisfy]=func_suff_condition_optSIC(B,U,M,h,h_cell,P_max,N0);
fprintf('Total number of user pairs in each cell:\n\n')
%Total number of user pairs in each cell: M!/(M-2)!2!
for b=1:B
    Tot_pairs(b)=U(b)*(U(b)-1)/2;
end
disp(Tot_pairs)
fprintf('Total number of user pairs violating the sufficient condition in each cell:\n\n')
disp(num_pairs_unsatisfy)
fprintf('Percentage of user pairs which cannot satisfy the sufficient condition:\n\n')
disp(perc_pair_unsatisfy)

if max(perc_pair_unsatisfy)>0
    fprintf('---The CNR-based decoding order "may" not be optimal!\n\n')
else 
    fprintf('---The CNR-based decoding order is optimal.\n\n')
%% Total Power Minimization Problem: Optimal joint power allocation and SIC ordering (JSPA)
fprintf('***Power Minimization Problem: Optimal JSPA:***\n\n\n')
eps_tol=1e-6; %stopping criterion
alpha_initial=zeros(1,B); %alpha_initial=0 (Tightenning the Lower Bound)
[alpha,p,P_tot_iter,iteration_num,SIC_order,h_norm,Feasible_JSPA,diverges]=...
    func_alg_minimize_pow(B,U,M,h,h_cell,N0,R_min,P_max,ICI_indctr,eps_tol,alpha_initial);
alpha_opt_PM=alpha; %optimal alpha
p_opt_PM=p; %optimal power
P_tot_opt_PM=P_tot_iter(end,end); %total power at the converged point
SIC_order_opt_PM=SIC_order; % optimal decoding order (at the converged point)
if diverges==1
    fprintf('---Diverged >>> Infeasible without power constraint---\n\n')
elseif diverges==0 && Feasible_JSPA==0
    fprintf('---Converged >>> but infeasible (Not Enough Power!)---\n\n')
    fprintf('Minimum alpha:\n\n')
    disp(alpha)
elseif Feasible_JSPA==1
    fprintf('---Feasible!---\n\n')
end

%% Power Minimization Problem: Optimal joint rate and power allocation (JRPA) with CNR-based decoding order 
Feasible_JRPA=0;
if (diverges==0)
    fprintf('\n***Power Minimization Problem: CNR-based decoding order with Optimal JRPA***\n\n\n')
    %Solving...
    h_norm=h_cell./(N0); %normalized channel gains by noise (within the cells)
    SIC_order=SIC_CNR; %CNR-based decoding order 
    lambda=lambda_CNR; %cancellation decision indicator according to the CNR-based decoding order 
    p=[]; ICI=[];
    cvx_begin quiet
    cvx_precision best
        variable p(B,M)
        subject to
%         %power constraint
%         sum(p,2)<=P_max';
        %Min rate constraint (main)
        ICI=reshape(sum(repmat(sum(p,2),1,B,M).*ICI_indctr.*h,1),B,M); %Inter-cell Interference
        for b=1:B
            for m=1:U(b)
                1e+15*p(b,m).*h_cell(b,m) >= 1e+15*(2.^R_min(b,m) - 1) .* (sum(reshape(lambda(b,m,:),1,M).*p(b,:)) .* h_cell(b,m) ...
                    + ICI(b,m) + N0(b,m));
            end
        end    
        %SIC constraint
        for b=1:B
            for i=1:U(b)
                for k=1:U(b)
                    if lambda(b,i,k)==1
                        1e+15*p(b,i).*h_cell(b,k) >= 1e+15*(2.^R_min(b,i) - 1) .* (sum(reshape(lambda(b,i,:),1,M).*p(b,:)) .* h_cell(b,k) ...
                            + ICI(b,k) + N0(b,k));
                    end
                end
            end
        end
        %Positive power
        p>=0;
        minimize sum(sum(p))   
    cvx_end
    if min(isfinite(p),[],'all')==1
        p_opt_JRPA_PM=p;
        alpha_JRPA_PM=sum(p,2)'./P_max; %optimal alpha
        Feasible_JRPA=(max(alpha_JRPA_PM)<=1);
        P_tot_JRPA_PM=sum(p,'all');
        P_tot_gap_JRPA=100*(P_tot_JRPA_PM-P_tot_opt_PM)./P_tot_opt_PM;
        if Feasible_JRPA==1
            fprintf('---Feasible!---\n\n')
        else fprintf('---Infeasible! Not enough power!---\n\n')
        end
    else fprintf('---Outage is occurred without power constraint\n\n')
    end
end

%% Sum-rate Maximization Problem: Optimal JSPA << Benchmark
clear p alpha ICI
CNR_opt=0;
if (Feasible_JRPA==1)
    fprintf('\n***Sum-rate Maximization Problem: Optimal JSPA\n\n\n');
    %statistics for computation time
    eps_a=1e-3; %stepsize alpha
    alpha_min=alpha_opt_PM;
    ceil_amin=ceil(alpha_min*ceil(inv(eps_a)))./inv(eps_a); %vector of minimum alpha vector
    [p_opt,r_opt,alpha_opt,SIC_order_opt,R_opt]=...
        func_alg_maximize_sumrate(B,U,M,R_min,P_max,ceil_amin,ICI_indctr,h,h_cell,N0,eps_a);
    p_opt_sumrate=p_opt;
    r_opt_sumrate=r_opt;
    alpha_opt_sumrate=alpha_opt;
    SIC_order_opt_sumrate=SIC_order_opt;
    R_opt_sumrate=R_opt;
    if R_opt==0
        fprintf('---Feasible solution is not found! Reduce eps_a!\n\n')
    else
        %%%%%Results
        fprintf('Optimal SIC ordering:\n')
        disp(SIC_order_opt_sumrate)
        fprintf('Spectral efficiency of users (bps/Hz):\n')
        disp(r_opt_sumrate)
        fprintf('Total spectral efficiency of users (bps/Hz):\n')
        disp(R_opt_sumrate)
        fprintf('CNR-based SIC ordering:\n')
        disp(SIC_CNR)
        fprintf('SIC ordering differences matrix between optimal and heuristic:\n')
        disp(SIC_order_opt_sumrate~=SIC_CNR)
        fprintf('Percentage of user pairs with incorrect decoding order in each cell:\n')
        SIC_order=SIC_order_opt_sumrate;
        [lambda]=func_Lambda(B,U,M,SIC_order);
        lambda_opt=lambda;
        for b=1:B
            if U(b)>=2
                tot_pairs(b)=U(b)*(U(b)-1)/2; %number of total pairs
            else tot_pairs(b)=1;
            end
            num_inc_pair_sumrate(b)=sum(nonzeros(lambda_CNR(b,:,:)~=lambda_opt(b,:,:)),'all')./2; %number of incorrect orders
            perc_inc_order_sumrate(b)=100*(num_inc_pair_sumrate(b))./tot_pairs(b);
        end
        disp(perc_inc_order_sumrate)
        CNR_opt=(max(perc_inc_order_sumrate)==0);
        if CNR_opt==1
            fprintf('---The optimality of CNR-based decoding order is verified!\n\n')
        else fprintf('---CNR-based decoding order is not optimal!\n\n')
        end
        save('STEP1_GeneralCode_MultiCell_NOMA','Dis_BS','radius_BS','Cor_BS','Cor_user','B','U','M','N0','h','h_cell',...
                'ICI_indctr','P_max','R_min','SIC_CNR','lambda_CNR','Feasible_JRPA',...
                'CNR_opt','tt_max','SIC_order_opt_sumrate','r_opt_sumrate',...
                'R_opt_sumrate','p_opt_sumrate','alpha_opt_sumrate','p_opt_JRPA_PM')
    end
end

end