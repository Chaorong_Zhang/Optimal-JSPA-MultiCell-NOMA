%% Figure of Network Topology
clear all
clc
load('STEP1_GeneralCode_MultiCell_NOMA','Dis_BS','radius_BS','Cor_BS','Cor_user','B','U','M')
%Coverage of BSs
Fig_topology_JRPAconverge=figure;
hold on
t=0:0.1:6.3;

Leg_coverage_MBS=plot(radius_BS(1).*sin(t)+real(Cor_BS(1)),radius_BS(1).*cos(t)+imag(Cor_BS(1)),'LineStyle','-.','Color',[0 0 0],'linewidth',2);axis equal
Leg_coverage_FBS=plot(radius_BS(2).*sin(t)+real(Cor_BS(2)),radius_BS(2).*cos(t)+imag(Cor_BS(2)),'LineStyle','--','Color',[0 0 0.5],'linewidth',0.01);axis equal

%Location of BSs
Leg_MBS=plot(real(Cor_BS(1)),imag(Cor_BS(1)),'MarkerFaceColor',[0 0 0],'MarkerEdgeColor','none',...
    'MarkerSize',10,'Marker','d','LineStyle','none','Color',[0 0 0]);
Leg_FBS=plot(real(Cor_BS(2)),imag(Cor_BS(2)),'MarkerFaceColor',[0 0 0],'MarkerEdgeColor','none',...
    'MarkerSize',8,'Marker','o','LineStyle','none','Color',[0 0 0]);

%Location of MBSusers
for b=1:1
    for m=1:U(b)
        Leg_MBSuser=plot(real(Cor_user(b,m)),imag(Cor_user(b,m)),'MarkerFaceColor',[0 0 0],...
            'MarkerEdgeColor',[0 0 0],...
            'MarkerSize',10,...
            'Marker','x',...
            'LineWidth',2,...
            'LineStyle','none',...
            'Color',[0 0 0]);
    end
end
%Location of FBSusers
for b=2:2
    for m=1:U(b)
        Leg_FBSuser=plot(real(Cor_user(b,m)),imag(Cor_user(b,m)),'MarkerFaceColor',[0 0.447058826684952 0.74117648601532],...
            'MarkerEdgeColor',[0 0.447058826684952 0.74117648601532],...
            'MarkerSize',7,...
            'Marker','*',...
            'LineWidth',1,...
            'LineStyle','none',...
            'Color',[0 0 0]);
    end
end
xlabel('Horizontal axis coordinate (m)','FontSize',14,...
    'FontName','Times New Roman')
ylabel('Vertical axis coordinate (m)','FontSize',14,...
    'FontName','Times New Roman')
% Create legend
legend1 = legend([Leg_coverage_MBS,Leg_coverage_FBS,Leg_MBS,Leg_FBS,Leg_MBSuser,Leg_FBSuser],...
    'Coverage area of MBS','Coverage area of FBS','MBS','FBS','Macro-cell user','Femto-cell user');
set(legend1,'FontSize',12,'FontName','Times New Roman','EdgeColor',[0 0 0])
set(gca,'XTick',[-500:200:500],'YTick',[-500:100:500],'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_topology_JRPAconverge,'Fig_topology_JRPAconverge')

%% Users Spectral Efficiency: Equal power allocation (EPA)
clear all
clc
load('STEP1_GeneralCode_MultiCell_NOMA','B','U')
load('STEP2_Sequential_Program_EPA','R_user_EPA','R_tot_EPA','p_EPA',...
        'Failure_EPA','Failure_EPA_CVX')
if Failure_EPA==0
    iter_max=length(R_tot_EPA);
    iterations=[0:iter_max-1];
    for b=1:B
        for i=1:U(b)
            for tt=1:iter_max
                Rate_user(tt,b,i)=R_user_EPA{tt}(b,i);
            end
        end
    end
    Fig_convergenceJRPA_userrate_equalpow=figure;
    hold on
    plot(iterations,Rate_user(:,2,1)','MarkerFaceColor','none','MarkerEdgeColor','b',...
        'MarkerSize',6,'Marker','>','Color','k','linestyle','--');
    plot(iterations,Rate_user(:,2,2)','MarkerFaceColor','none','MarkerEdgeColor','b',...
        'MarkerSize',6,'Marker','o','Color','k','linestyle','--');
    plot(iterations,Rate_user(:,1,1)','MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',6,'Marker','>','Color','k','linestyle','-');
    plot(iterations,Rate_user(:,1,2)','MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',6,'Marker','o','Color','k','linestyle','-');
    plot(iterations,Rate_user(:,1,3)','MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',6,'Marker','square','Color','k','linestyle','-');
    legend('Femtocell user 1','Femtocell user 2','Macrocell user 1',...
        'Macrocell user 2','Macrocell user 3','location','northeast','NumColumns',1);
    set(legend,'FontSize',14,'FontName','Times New Roman','EdgeColor',[0 0 0],'location','east')     
    xlabel('Iteration index','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Spectral efficiency of each user (bps/Hz)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',[0:5:iter_max],'YTick',[0:max(R_tot_EPA)],'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_convergenceJRPA_userrate_equalpow,'Fig_convergenceJRPA_userrate_equalpow')
end

%% Users Spectral Efficiency: Approximated Rate Function (ARF)
clear all
clc
load('STEP1_GeneralCode_MultiCell_NOMA','B','U')
load('STEP2_Sequential_Program_ARF','Failure_ARF')
if Failure_ARF==0
    load('STEP2_Sequential_Program_ARF','R_user_ARF','R_tot_ARF','p_ARF',...
                'Failure_ARF','Failure_ARF_CVX')
    iter_max=length(R_tot_ARF);
    iterations=[0:iter_max-1];
    for b=1:B
        for i=1:U(b)
            for tt=1:iter_max
                Rate_user(tt,b,i)=R_user_ARF{tt}(b,i);
            end
        end
    end
    Fig_convergenceJRPA_userrate_mr=figure;
    hold on
    plot(iterations,Rate_user(:,2,1)','MarkerFaceColor','none','MarkerEdgeColor','b',...
        'MarkerSize',6,'Marker','>','Color','k','linestyle','--');
    plot(iterations,Rate_user(:,2,2)','MarkerFaceColor','none','MarkerEdgeColor','b',...
        'MarkerSize',6,'Marker','o','Color','k','linestyle','--');
    plot(iterations,Rate_user(:,1,1)','MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',6,'Marker','>','Color','k','linestyle','-');
    plot(iterations,Rate_user(:,1,2)','MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',6,'Marker','o','Color','k','linestyle','-');
    plot(iterations,Rate_user(:,1,3)','MarkerFaceColor','none','MarkerEdgeColor','k',...
        'MarkerSize',6,'Marker','square','Color','k','linestyle','-');
    legend('Femtocell user 1','Femtocell user 2','Macrocell user 1',...
        'Macrocell user 2','Macrocell user 3','location','northeast','NumColumns',1);
    set(legend,'FontSize',14,'FontName','Times New Roman','EdgeColor',[0 0 0],'location','east')     
    xlabel('Iteration index','FontSize',16,...
        'FontName','Times New Roman')
    ylabel('Spectral efficiency of each user (bps/Hz)','FontSize',16,...
        'FontName','Times New Roman')
    set(gca,'XTick',[0:5:iter_max],'YTick',[0:max(R_tot_ARF)],'FontName','Times New Roman','FontSize',14)
    grid on
    savefig(Fig_convergenceJRPA_userrate_mr,'Fig_convergenceJRPA_userrate_mr')
end

%% Users Spectral Efficiency: Minimum Rate Equality
clear all
clc
load('STEP1_GeneralCode_MultiCell_NOMA','B','U')
load('STEP2_Sequential_Program_MRE','R_user_MRE','R_tot_MRE','p_MRE','Failure_MRE_CVX')
iter_max=length(R_tot_MRE);
iterations=[0:iter_max-1];
for b=1:B
    for i=1:U(b)
        for tt=1:iter_max
            Rate_user(tt,b,i)=R_user_MRE{tt}(b,i);
        end
    end
end
Fig_convergenceJRPA_userrate_equalr=figure;
hold on
plot(iterations,Rate_user(:,2,1)','MarkerFaceColor','none','MarkerEdgeColor','b',...
    'MarkerSize',6,'Marker','>','Color','k','linestyle','--');
plot(iterations,Rate_user(:,2,2)','MarkerFaceColor','none','MarkerEdgeColor','b',...
    'MarkerSize',6,'Marker','o','Color','k','linestyle','--');
plot(iterations,Rate_user(:,1,1)','MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',6,'Marker','>','Color','k','linestyle','-');
plot(iterations,Rate_user(:,1,2)','MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',6,'Marker','o','Color','k','linestyle','-');
plot(iterations,Rate_user(:,1,3)','MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',6,'Marker','square','Color','k','linestyle','-');
legend('Femtocell user 1','Femtocell user 2','Macrocell user 1',...
    'Macrocell user 2','Macrocell user 3','location','northeast','NumColumns',1);
set(legend,'FontSize',14,'FontName','Times New Roman','EdgeColor',[0 0 0],'location','east')     
xlabel('Iteration index','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Spectral efficiency of each user (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',[0:5:iter_max],'YTick',[0:max(R_tot_MRE)],'FontName','Times New Roman','FontSize',14)
grid on
savefig(Fig_convergenceJRPA_userrate_equalr,'Fig_convergenceJRPA_userrate_equalr')

%% Total Spectral Efficiency Comparision: All the three approaches
%%Plot Figure Convergence
clear all
clc
load('STEP1_GeneralCode_MultiCell_NOMA','B','U','R_opt_sumrate','tt_max')
load('STEP2_Sequential_Program_EPA','R_tot_EPA','Failure_EPA')
load('STEP2_Sequential_Program_ARF','R_tot_ARF','Failure_ARF')
load('STEP2_Sequential_Program_MRE','R_tot_MRE')
iterations_EPA=[1:length(R_tot_EPA)]-1;
iterations_ARF=[1:length(R_tot_ARF)]-1;
iterations_MRE=[1:length(R_tot_MRE)]-1;
R_opt_vec=R_opt_sumrate*ones(1,tt_max);
iterations=[1:tt_max]-1;
Fig_convergenceJRPA_totrate=figure;
hold on
plot(iterations,R_opt_vec,'linewidth',3,'Color','r','linestyle','-');
plot(iterations_ARF,R_tot_ARF,'MarkerFaceColor','none','MarkerEdgeColor','k',...
    'MarkerSize',6,'Marker','>','Color','k','linestyle','-');
plot(iterations_EPA,R_tot_EPA,'MarkerFaceColor','none','MarkerEdgeColor','r',...
    'MarkerSize',6,'Marker','o','Color','r','linestyle','-');
plot(iterations_MRE,R_tot_MRE,'MarkerFaceColor','none','MarkerEdgeColor','b',...
    'MarkerSize',6,'Marker','square','Color','b','linestyle','-');
legend('Optimal Value','Approximated Rate Function','Equal Power Allocation','Minimum Rate Equality',...
       'location','southeast','NumColumns',1);
set(legend,'FontSize',14,'FontName','Times New Roman','EdgeColor',[0 0 0])     
xlabel('Iteration index','FontSize',16,...
    'FontName','Times New Roman')
ylabel('Total spectral efficiency of users (bps/Hz)','FontSize',16,...
    'FontName','Times New Roman')
set(gca,'XTick',[0:5:60],'FontName','Times New Roman','FontSize',14)
grid on
% creating the zoom-in inset
ax=axes();
set(ax,'units','normalized','position',[0.25,0.5,0.2,0.2])
box(ax,'on')
your_index= 0<=iterations & iterations<=6;
plot(iterations(your_index),R_opt_vec(your_index),'r-',...
    iterations(your_index),R_tot_ARF(your_index),'k->',...
    iterations(your_index),R_tot_EPA(your_index),'r-o',...
    iterations(your_index),R_tot_MRE(your_index),'b-square');

axis tight
set(gca,'FontName','Times New Roman','FontSize',10)
grid on

savefig(Fig_convergenceJRPA_totrate,'Fig_convergenceJRPA_totrate')