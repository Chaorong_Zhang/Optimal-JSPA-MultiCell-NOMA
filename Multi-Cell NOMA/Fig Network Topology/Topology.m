clear all
clc
%% Simulation Settings
%%%%%Network Topology
%BS placement
Dis_BS=200; %BS to center
Cor_BS=func_CoordinateBS(Dis_BS); %Coordinate of BSs
B=numel(Cor_BS); %Number of BSs
%User placement
radius_BS=[500,40*ones(1,B-1)]; %radius of each BS in meter
%Number of users in each cell
Macro_cell_users=3;
Femto_cell_users=2;
U=[Macro_cell_users,Femto_cell_users*ones(1,B-1)];
M=max(U);
min_dis_user=[20,2*ones(1,B-1)]; %minimum distance of user to associated BS in meter
%%%%%Coordinate of users
Cor_user=func_CoordinateUser(B,Cor_BS,U,M,radius_BS,min_dis_user);
%% Figure of Network Topology
[Leg_coverage_MBS]=func_FigTopology(B,U,radius_BS,Cor_BS,Cor_user);
clear Dis_BS Cor_BS radius_BS min_Dis_User Cor_user Leg_coverage_BS b F_cell ...
  M_cell R_macro R_femto Femto_cell Macro_cell mean_N0 P_MBS P_FBS
