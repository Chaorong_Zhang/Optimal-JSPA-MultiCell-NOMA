function ICI_indctr=func_ICI_indicator(B,M)
ICI_indctr=zeros(B,B,M);
for j=1:B
    for b=1:B
        for i=1:M
            if (j~=b)
                ICI_indctr(j,b,i)=1;
            end
        end
    end
end